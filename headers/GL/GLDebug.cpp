/**

Add the folowing to GL_Init just after you initialise glew

#ifdef _DEBUG
    // Initialise debug callback
    GLDebug_Init( );
#endif

Add the following to main just after you set teh opengl to 4.4 core

#ifdef _DEBUG
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG );
#endif

*/

// Using SDL, SDL OpenGL, GLEW
#define GLEW_STATIC
#include <GL/glew.h>
#include <SDL.h>
#include <SDL_opengl.h>

#include <string>

void APIENTRY DebugCallback( uint32_t uiSource, uint32_t uiType, uint32_t uiID, uint32_t uiSeverity, int32_t iLength, const char * p_cMessage, void* p_UserParam )
{
    // Get the callback message
    std::string sLogMessage = "OpenGL Debug: Severity=";

    // Add the severity
    switch( uiSeverity )
    {
    case GL_DEBUG_SEVERITY_HIGH:
    {
        sLogMessage += "High";
        break;
    }
    case GL_DEBUG_SEVERITY_MEDIUM:
    {
        sLogMessage += "Medium";
        break;
    }
    case GL_DEBUG_SEVERITY_LOW:
    {
        sLogMessage += "Low";
        break;
    }
    case GL_DEBUG_SEVERITY_NOTIFICATION:
    {
        sLogMessage += "Notification";
        break;
    }
    default:
        break;
    }

    // Add the error type
    sLogMessage += " Type=";
    switch( uiType )
    {
    case GL_DEBUG_TYPE_ERROR:
    {
        sLogMessage += "Error";
        break;
    }
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
    {
        sLogMessage += "Deprecated";
        break;
    }
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
    {
        sLogMessage += "Undefined";
        break;
    }
    case GL_DEBUG_TYPE_PORTABILITY:
    {
        sLogMessage += "Portability";
        break;
    }
    case GL_DEBUG_TYPE_PERFORMANCE:
    {
        sLogMessage += "Performance";
        break;
    }
    case GL_DEBUG_TYPE_OTHER:
    {
        sLogMessage += "Other";
        break;
    }
    default:
        break;
    }

    // Add the source
    sLogMessage += " Source=";
    switch( uiSource )
    {
    case GL_DEBUG_SOURCE_API:
    {
        sLogMessage += "OpenGL";
        break;
    }
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
    {
        sLogMessage += "OS";
        break;
    }
    case GL_DEBUG_SOURCE_SHADER_COMPILER:
    {
        sLogMessage += "GLSL Compiler";
        break;
    }
    case GL_DEBUG_SOURCE_THIRD_PARTY:
    {
        sLogMessage += "3rd Party";
        break;
    }
    case GL_DEBUG_SOURCE_APPLICATION:
    {
        sLogMessage += "Application";
        break;
    }
    case GL_DEBUG_SOURCE_OTHER:
    {
        sLogMessage += "Other";
        break;
    }
    default:
        break;
    }

    // Add the actual log message
    sLogMessage += " - ";
    sLogMessage += p_cMessage;

    // Output to the Log
    if( uiSeverity == GL_DEBUG_SEVERITY_HIGH )
    {
        SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION, sLogMessage.c_str( ) );

        //This a serious error so we need to shutdown the program
        SDL_Quit( );
    }
    else
    {
        SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION, sLogMessage.c_str( ) );
    }
}

void GLDebug_Init( )
{
#ifdef _DEBUG
    //Allow for synchronous callbacks. This will impact performance but it allows for the immediate 
    //   call of the callback function.
    glEnable( GL_DEBUG_OUTPUT_SYNCHRONOUS );

    //Set up the debug info callback
    glDebugMessageCallback( (GLDEBUGPROC)&DebugCallback, NULL );

    //Set up the type of debug information we want to receive
    uint32_t uiUnusedIDs = 0;
    glDebugMessageControl( GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, &uiUnusedIDs, GL_TRUE ); //Enable all
    glDebugMessageControl( GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_NOTIFICATION, 0, NULL, GL_FALSE ); //Disable notifications
    //glDebugMessageControl( GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_LOW, 0, NULL, GL_FALSE ); //Disable low severity
    //glDebugMessageControl( GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_MEDIUM, 0, NULL, GL_FALSE ); //Disable medium severity warnings
#endif
}