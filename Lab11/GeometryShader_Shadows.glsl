#version 440
#define MAX_LIGHTS 32

layout( binding = 0 ) uniform CameraData
{
	mat4 m4ViewProjection;
	vec3 v3CameraPosition;
};
layout( binding = 6 ) uniform CameraShadowData
{
	mat4 m4ViewProjectionShadow[MAX_LIGHTS];
};

layout( location = 0 ) uniform int iNumLights;

layout( triangles, invocations = MAX_LIGHTS ) in;
layout( triangle_strip, max_vertices = 3 ) out;

layout( location = 0 ) in vec4 v4VertexPos[];

void main()
{
	// Check if valid invocation
	if( gl_InvocationID < iNumLights )
	{
		// Check front face culling
		vec3 v3Normal = cross( v4VertexPos[2].xyz - v4VertexPos[0].xyz, v4VertexPos[0].xyz - v4VertexPos[1].xyz );
		vec3 v3ViewDirection = v3CameraPosition.xyz - v4VertexPos[0].xyz;

		vec4 v4PositionVPTemp[3];
		int iOutOfBound[6] = int[6]( 0, 0, 0, 0, 0, 0 );
		// Loop over each vertex and get clip space position
		for( int i = 0; i < 3; ++i )
		{
			// Transform position
			v4PositionVPTemp[i] = m4ViewProjectionShadow[gl_InvocationID] * v4VertexPos[i];

			// Check if any value is outside clip planes
			if(v4PositionVPTemp[i].x> v4PositionVPTemp[i].w) ++iOutOfBound[0];
			if(v4PositionVPTemp[i].x<-v4PositionVPTemp[i].w) ++iOutOfBound[1];
			if(v4PositionVPTemp[i].y> v4PositionVPTemp[i].w) ++iOutOfBound[2];
			if(v4PositionVPTemp[i].y<-v4PositionVPTemp[i].w) ++iOutOfBound[3];
			if(v4PositionVPTemp[i].z> v4PositionVPTemp[i].w) ++iOutOfBound[4];
			if(v4PositionVPTemp[i].z<-v4PositionVPTemp[i].w) ++iOutOfBound[5];
		}

		// Loop over each clip face and check if triangle is entirely outside
		bool bInFrustum = true;
		for( int i = 0; i < 6; ++i )
			if( iOutOfBound[i] == 3 ) bInFrustum = false;

		// Loop over each vertex in the face and output
		//if(bInFrustum && ( dot( v3Normal, v3ViewDirection ) < 0 ))
		if(bInFrustum)
		{
			for( int i = 0; i < 3; ++i )
			{
				// Transform Position
				gl_Position = m4ViewProjectionShadow[gl_InvocationID] * v4VertexPos[i];

				// Output to array layer based on invocation ID
				gl_Layer = gl_InvocationID;
				EmitVertex( );
			}
			EndPrimitive( );
		}
	}
}