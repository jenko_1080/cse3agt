#version 440

layout( binding = 7 ) uniform InvResolution {
	vec2 v2InvResolution;
};

layout( binding = 15 ) uniform sampler2D s2AccumulationTexture;
layout( location = 0 ) out float fYOut;
layout( location = 1 ) out vec3 fYBloomOut;

void main( ) {
	// Get UV coordinates
	vec2 v2UV = gl_FragCoord.xy * v2InvResolution;
	// Get colour data
	vec3 v3AccumColour = texture( s2AccumulationTexture, v2UV ).rgb;
	// Calculate luminance 
	const vec3 v3LuminanceConvert = vec3( 0.2126, 0.7152, 0.0722 );
	float fY = dot( v3AccumColour, v3LuminanceConvert );

	// Calculate log luminance 
	const float fEpsilon = 0.00000001;
	float fYoutTemp = log( fY + fEpsilon );

	const float fYwhite = 0.22;
	
	// Perform range mapping (optional: only needed with integer textures)
	fYoutTemp = ( fYoutTemp - log( fEpsilon ) ) / ( log( fYwhite ) - log( fEpsilon ) );

	// Output bloom values
	fYBloomOut = ( fY >= fYwhite * 1.25 )? v3AccumColour : vec3( 0 );

	fYOut = fYoutTemp;
}