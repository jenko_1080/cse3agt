#version 440

layout(location = 0) in vec2 v2VertexPos;

void main()
{
	gl_Position = vec4(v2VertexPos, 0.0, 1.0);
}