#version 440
// Defines //
/////////////
#define M_RCPPI 0.31830988618379067153776752674503
#define M_PI 3.1415926535897932384626433832795

// Data Structures //
/////////////////////

layout( binding = 0 ) uniform CameraData
{
	mat4 m4ViewProjection;
	vec3 v3CameraPosition;
};
layout( binding = 3 ) uniform ReflectPlaneData // UBO for data required to map the texture to the planes surface
{
	mat4 m4ReflectVP;
};


// Add in uniforms for textures (Layouts for easy init in main program)
layout( location = 1 ) uniform float fEmissivePower;

layout( binding = 0 ) uniform sampler2D s2DiffuseTexture; 
layout( binding = 1 ) uniform sampler2D s2SpecularTexture; 
layout( binding = 2 ) uniform sampler2D s2RoughnessTexture;
layout( binding = 3 ) uniform samplerCube scRefractMapTexture; // Refraction / Transparency cubemap
layout( binding = 4 ) uniform sampler2D s2ReflectTexture; // Create additional texture input for object's reflection texture
layout( binding = 5 ) uniform samplerCube scReflectMapTexture; // Create additional texture input for object's cubemap reflection texture
//layout( binding = 6 ) uniform sampler2DArrayShadow s2aShadowTexture; // Spot light shadows
//layout( binding = 7 ) uniform samplerCubeArrayShadow scaPointShadowTexture; // Point light shadows
//layout( binding = 8 ) uniform sampler2D transparentTexture; // TODO: FOR TRANSPARENT MATERIALS
layout( binding = 9 ) uniform sampler2D s2NormalTexture;
layout( binding = 10 ) uniform sampler2D s2BumpTexture;

// Program Inputs
layout( location = 0 ) in vec4 v4Position;
layout( location = 1 ) in vec4 v4Normal;
layout( location = 2 ) in vec2 v2UV;
layout( location = 3 ) in vec4 v4Tangent;

// Program Outputs
layout( location = 0 ) out vec3 v3AccumulationOut;
layout( location = 1 ) out vec3 v3NormalOut;
layout( location = 2 ) out vec3 v3DiffuseOut;
layout( location = 3 ) out vec4 v4SpecularRoughOut;


// Temporary vars
// Size of bump map
const float fBumpScale = 0.01;


subroutine vec3 Emissive( vec3, vec4 ); // Set up subroutine for emissive objects
layout( location = 0 ) subroutine uniform Emissive EmissiveUniform; // Set up subroutine for emissive objects
subroutine vec3 RefractMap( vec3, vec3, vec3, vec4, vec4 ); // Set up subroutine for transparent materials
layout( location = 1 ) subroutine uniform RefractMap RefractMapUniform;
subroutine vec3 ReflectMap( vec3, vec3, vec3, vec4, float ); // Set up subroutine for reflective materials
layout( location = 2 ) subroutine uniform ReflectMap ReflectMapUniform;

// Function Prototypes //
/////////////////////////
vec3 schlickFresnel( in vec3 v3LightDirection, in vec3 v3HalfVector, in vec4 v4SpecularColour );
float GGXVisibility( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in float fRoughness );	// for GGX
vec3 SpecularTransmit( in vec3 v3Normal, in vec3 v3ViewDirection, in vec4 v4DiffuseColour, in vec4 v4SpecularColour );
vec3 GGXReflect( in vec3 v3Normal, in vec3 v3ReflectDirection, in vec3 v3ViewDirection, in vec3 v3ReflectRadiance, in vec4 v4SpecularColour, in float fRoughness );
vec3 normalMap( in vec3 v3Normal, in vec3 v3Tangent, in vec3 v3BiTangent, in vec2 v2LocalUV );
vec2 parallaxMap( in vec3 v3Normal, in vec3 v3Tangent, in vec3 v3BiTangent, in vec3 v3ViewDirection );

// Subroutines //
/////////////////
// Emissive materials
layout( index = 0 ) subroutine(Emissive) vec3 noEmissive( vec3 v3ColourOut, vec4 v4DiffuseColour )
{
	return v3ColourOut; // Return colour unmodified
}
layout( index = 1 ) subroutine(Emissive) vec3 textureEmissive( vec3 v3ColourOut, vec4 v4DiffuseColour )
{
	return v3ColourOut + ( fEmissivePower * v4DiffuseColour.xyz ); // Add in emissive contribution
}
// Transparent materials
layout( index = 2 ) subroutine(RefractMap) vec3 noRefractMap( vec3 v3ColourOut, vec3 v3Normal, vec3 v3ViewDirection, vec4 v4DiffuseColour, vec4 v4SpecularColour )
{
	return v3ColourOut; // Return colour unmodified
}
layout( index = 3 ) subroutine(RefractMap) vec3 textureRefractMap( vec3 v3ColourOut, vec3 v3Normal, vec3 v3ViewDirection, vec4 v4DiffuseColour, vec4 v4SpecularColour )
{
	// Get specular transmittance term
	vec3 v3Transmit = SpecularTransmit( v3Normal, v3ViewDirection, v4DiffuseColour, v4SpecularColour );

	// Add in transparent contribution and blend with existing
	return mix( v3Transmit, v3ColourOut, v4DiffuseColour.w );
}

// Reflections, planar surfaces get texture reflection - curved get a cubemap
layout( index = 4 ) subroutine(ReflectMap) vec3 noReflectMap( vec3 v3ColourOut, vec3 v3Normal, vec3 v3ViewDirection, vec4 v4SpecularColour, float fRoughness )
{
	return v3ColourOut; // Return colour unmodified
}
layout( index = 5 ) subroutine(ReflectMap) vec3 textureReflectPlane( vec3 v3ColourOut, vec3 v3Normal, vec3 v3ViewDirection, vec4 v4SpecularColour, float fRoughness )
{
	// UV calculations
	vec4 v4RVPPosition = m4ReflectVP * v4Position;
	vec2 v2ReflectUV = v4RVPPosition.xy / v4RVPPosition.w;
	v2ReflectUV = ( v2ReflectUV + 1.0 ) * 0.50;

	// LOD offset and texture lookup
	// Calculate LOD offset
	float fLOD = textureQueryLod( s2ReflectTexture, v2ReflectUV ).y;
	float fGloss = 1.0 - fRoughness;
	fLOD += ( ( 2.0 / ( fGloss * fGloss ) ) - 1.0 ) * max( fLOD, 1.0 );

	vec3 v3ReflectRadiance = textureLod( s2ReflectTexture, v2ReflectUV, fLOD ).xyz;
	
	// Reflection direction calculation
	vec3 v3ReflectDirection = normalize( reflect( -v3ViewDirection, v3Normal ) );

	// Shading
	vec3 v3RetColour = GGXReflect( v3Normal, v3ReflectDirection, v3ViewDirection, v3ReflectRadiance, v4SpecularColour, fRoughness );
	
	return v3ColourOut + v3RetColour;
}
layout( index = 6 ) subroutine(ReflectMap) vec3 textureReflectCube( vec3 v3ColourOut, vec3 v3Normal, vec3 v3ViewDirection, vec4 v4SpecularColour, float fRoughness )
{
	// Get reflect direction
	vec3 v3ReflectDirection = normalize( reflect( -v3ViewDirection, v3Normal ) );

	// Calculate LOD offset
	float fLOD = textureQueryLod( scReflectMapTexture, v3ReflectDirection ).y;
	float fGloss = 1.0 - fRoughness;
	fLOD += ( ( 2.0 / ( fGloss * fGloss ) ) - 1.0 ) * max( fLOD, 1.0 );

	// Get reflect texture data
	vec3 v3ReflectRadiance = textureLod( scReflectMapTexture, v3ReflectDirection, fLOD ).xyz;

	// Perform shading
	vec3 v3RetColour = GGXReflect( v3Normal, v3ReflectDirection, v3ViewDirection, v3ReflectRadiance, v4SpecularColour, fRoughness );

	return v3ColourOut + v3RetColour;
}

// Main //
//////////
void main() {
	// Set up view direction
	vec3 v3TViewDirection = normalize( v3CameraPosition - v4Position.xyz );
	// Normalize the inputs
	vec3 v3TNormal = normalize( v4Normal.xyz );
	vec3 v3TTangent = normalize( v4Tangent.xyz );
	// Generate bitangent 
	vec3 v3BiTangent = cross( v3TNormal, v3TTangent );
	// Perform Parallax Occlusion Mapping 
	vec2 v2UVPO = parallaxMap( v3TNormal, v3TTangent, v3BiTangent, v3TViewDirection );
	// Perform Bump Mapping (now using parallax occlusion map)
	v3TNormal = normalMap( v3TNormal, v3TTangent, v3BiTangent, v2UVPO );
	// Get texture data
	vec4 v4DiffuseColour = texture( s2DiffuseTexture, v2UVPO );
	vec4 v4SpecularColour = texture( s2SpecularTexture, v2UVPO );
	float fRoughness = texture( s2RoughnessTexture, v2UVPO ).r;

	// Create / Initialise vars for colouring / lighting / whatever
	vec3 v3ColourOut = vec3(0.0, 0.0, 0.0);

	// Add Diffuse Lighting (ambient)
	v3ColourOut += v4DiffuseColour.xyz * vec3( 0.14 );
	// Add in any reflection contribution
	v3ColourOut = ReflectMapUniform( v3ColourOut, v3TNormal, v3TViewDirection, v4SpecularColour, fRoughness );
	// Add in any refraction contribution
	v3ColourOut = RefractMapUniform( v3ColourOut, v3TNormal, v3TViewDirection, v4DiffuseColour, v4SpecularColour );
	// Add in any emissive contribution
	v3ColourOut = EmissiveUniform( v3ColourOut, v4DiffuseColour );
	
	// Output to deferred G-Buffers
	v3AccumulationOut = v3ColourOut;
	v3NormalOut = v3TNormal;
	v3DiffuseOut = v4DiffuseColour.xyz;
	v4SpecularRoughOut = vec4( v4SpecularColour.xyz, fRoughness );
}

// Functions //
///////////////
vec3 schlickFresnel( in vec3 v3LightDirection, in vec3 v3HalfVector, in vec4 v4SpecularColour )
{
	// Schlick Fresnel approximation
	float fLH = dot( v3LightDirection, v3HalfVector );
	return v4SpecularColour.rgb + ( 1.0 - v4SpecularColour.rgb ) * pow( 1.0 - fLH, 5 );
}

float GGXVisibility( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in float fRoughness )
{
	// GGX Visibility function
	float fNL = max( dot( v3Normal, v3LightDirection ), 0.0 );
	float fNV = max( dot( v3Normal, v3ViewDirection ), 0.0 );
	float fRSq = fRoughness * fRoughness;
	float fRMod = 1.0 - fRSq;
	float fRecipG1 = fNL + sqrt( fRSq + ( fRMod * fNL * fNL ) );
	float fRecipG2 = fNV + sqrt( fRSq + ( fRMod * fNV * fNV ) );
	return 1.0 / ( fRecipG1 * fRecipG2 );
}

vec3 SpecularTransmit( in vec3 v3Normal, in vec3 v3ViewDirection, in vec4 v4DiffuseColour, in vec4 v4SpecularColour )
{
	// Calculate index of refraction from Fresnel term
	float fRootF0 = sqrt( v4SpecularColour.x );
	float fIOR = ( 1 - fRootF0 ) / ( 1 + fRootF0 );
	// Get refraction direction 
	vec3 v3Refract = refract( -v3ViewDirection, v3Normal, fIOR );
	// Get refraction map data 
	vec3 v3RefractColour = texture( scRefractMapTexture, v3Refract ).xyz;
	
	// Evaluate specular transmittance 
	vec3 v3RetColour = fIOR * ( 1.0 - schlickFresnel( v3Refract, -v3Normal, v4SpecularColour ) );
	v3RetColour *= v4DiffuseColour.xyz;
	// Combine with incoming light value 
	v3RetColour *= v3RefractColour;
	return v3RetColour; 
}

vec3 GGXReflect( in vec3 v3Normal, in vec3 v3ReflectDirection, in vec3 v3ViewDirection, in vec3 v3ReflectRadiance, in vec4 v4SpecularColour, in float fRoughness )
{
	// Calculate Torrence-Sparrow components
	vec3 v3F = schlickFresnel( v3ReflectDirection, v3Normal, v4SpecularColour );
	float fV = GGXVisibility( v3Normal, v3ReflectDirection, v3ViewDirection, fRoughness );

	// Combine specular
	vec3 v3RetColour = v3F * fV;

	// Modify by pdf
	v3RetColour *= ( 4 * dot( v3ViewDirection, v3Normal ) );

	// Multiply by view angle
	v3RetColour *= max( dot( v3Normal, v3ReflectDirection ), 0.0 );

	// Combine with incoming light value
	v3RetColour *= v3ReflectRadiance;	

	return v3RetColour;
}

vec3 normalMap( in vec3 v3Normal, in vec3 v3Tangent, in vec3 v3BiTangent, in vec2 v2LocalUV )
{
	// Get normal map value
	vec2 v2NormalMap = ( texture( s2NormalTexture, v2LocalUV ).rg - 0.5 ) * 2;
	vec3 v3NormalMap = vec3( v2NormalMap, sqrt( 1 - length( v2NormalMap ) ) );
	// Convert from tangent space
	vec3 v3RetNormal = mat3( v3Tangent.xy, v3Tangent.z, v3BiTangent.xy, v3BiTangent.z, v3Normal.xy, v3Normal.z ) * v3NormalMap;
	return normalize( v3RetNormal );
}

vec2 parallaxMap( in vec3 v3Normal, in vec3 v3Tangent, in vec3 v3BiTangent, in vec3 v3ViewDirection )
{
	// Get tangent space view direction
	vec3 v3TangentView = vec3( dot( v3ViewDirection, v3Tangent ), dot( v3ViewDirection, v3BiTangent ), dot( v3ViewDirection, v3Normal ) );
	v3TangentView = normalize( v3TangentView );
	// Get number of layers based on view direction
	const float fMinLayers = 5; 
	const float fMaxLayers = 15;
	float fNumLayers = round(mix(fMaxLayers,fMinLayers,abs(v3TangentView.z)));

	// Determine layer height
	float fLayerHeight = 1.0 / fNumLayers;
	// Determine texture offset per layer
	vec2 v2DTex = fBumpScale* v3TangentView.xy / (v3TangentView.z*fNumLayers);
	
	// Get texture gradients to allow for dynamic branching
	vec2 v2Dx = dFdx( v2UV );
	vec2 v2Dy = dFdy( v2UV );

	// Offset start position by half number of layers
	vec2 v2CurrUV = v2UV + ( v2DTex * fNumLayers * 0.5 );

	// Initialise height from texture
	float fCurrHeight = textureGrad( s2BumpTexture, v2CurrUV, v2Dx, v2Dy ).r;

	// Loop over each step until lower height is found
	float fViewHeight = 1.0;
	float fLastHeight;
	vec2 v2LastUV;
	for( int i = 0; i < int( fNumLayers ); i++ )
	{
		if( fCurrHeight >= fViewHeight )
			break;

		// Set current values as previous
		fLastHeight = fCurrHeight;
		v2LastUV = v2CurrUV;
		// Go to next layer
		fViewHeight -= fLayerHeight;
		// Shift UV coordinates
		v2CurrUV -= v2DTex;
		// Get new texture height
		fCurrHeight = textureGrad( s2BumpTexture, v2CurrUV, v2Dx, v2Dy ).r;
	}

	// Get heights for linear interpolation
	float fNextHeight = fCurrHeight - fViewHeight;
	float fPrevHeight = fLastHeight - ( fViewHeight + fLayerHeight );

	// Interpolate based on height difference
	float fWeight = fNextHeight / ( fNextHeight - fPrevHeight );
	return mix( v2CurrUV, v2LastUV, fWeight);
}