#version 440 

layout( binding = 0 ) uniform CameraData { 
	mat4 m4ViewProjection; 
	vec3 v3CameraPosition; 
	mat4 m4InvViewProjection; 
}; 

layout( binding = 7 ) uniform InvResolution { 
	vec2 v2InvResolution; 
}; 

layout( binding = 11 ) uniform sampler2D s2DepthTexture; 
layout( binding = 12 ) uniform sampler2D s2NormalTexture; 
out float fAOOut; 

// Size of AO radius 
const float fAORadius = 0.5; 
const float fEpsilon = 0.00000001; 

void main( ) { 
	// Get UV coordinates 
	vec2 v2UV = gl_FragCoord.xy * v2InvResolution; 

	// Get deferred data 
	float fDepth = texture( s2DepthTexture, v2UV ).r; 
	vec3 v3Normal = texture( s2NormalTexture, v2UV ).rgb; 
	
	// Calculate position from depth 
	fDepth = ( fDepth * 2.0f ) - 1.0f; 
	vec2 v2NDCUV = ( v2UV * 2.0f ) - 1.0f; 
	vec4 v4Position = m4InvViewProjection * vec4( v2NDCUV, fDepth, 1.0 ); 
	vec3 v3Position = v4Position.xyz / v4Position.w;

	// Define sampling values 
	const vec3 v3Samples[16] = vec3[]( 
		vec3( 0.000000000, -0.066666670, 0.188561812 ), 
		vec3( -0.200000003, 0.133333340, 0.319722116 ), 
		vec3( 0.300000012, -0.466666669, 0.228521824 ), 
		vec3( -0.600000024, -0.088888891, 0.521630883 ), 
		vec3( 0.009999999, 0.022222222, 0.031720228 ), 
		vec3( -0.059999998, -0.133333340, 0.190321371 ), 
		vec3( 0.330000013, 0.048888888, 0.286896974 ), 
		vec3( 0.079999998, -0.592592597, 0.228109658 ), 
		vec3( -0.314999998, -0.217777774, 0.747628152 ), 
		vec3( 0.050000000, 0.032592590, 0.053270280 ), 
		vec3( -0.174999997, -0.197037041, 0.094611868 ), 
		vec3( 0.180000007, -0.017777778, 0.444616646 ), 
		vec3( -0.085000000, 0.428148150, 0.521405935 ), 
		vec3( 0.769999981, -0.423703700, 0.044442899 ), 
		vec3( -0.112499997, 0.022222222, 0.035354249 ), 
		vec3( 0.019999999, 0.272592604, 0.166412979 ) ); 

	// Get 4x4 sample 
	ivec2 i2SamplePos = ivec2( gl_FragCoord.xy ) % 4; 
	vec3 v3Sample = v3Samples[ ( i2SamplePos.y * 4 ) + i2SamplePos.x ]; 

	// Determine world space tangent axis 
	vec3 v3SampleTangent = vec3( v3Sample.xy, 0.0 ); 
	vec3 v3Tangent = normalize( v3SampleTangent - v3Normal * dot( v3SampleTangent, v3Normal ) ); 
	vec3 v3BiTangent = cross( v3Normal, v3Tangent ); 
	mat3 m3TangentAxis = mat3( v3Tangent, v3BiTangent, v3Normal ); 

	float fAO = 0; 
	for( int i = 0; i < 16; i++ ) 
	{ 
		// Offset position along tangent plane 
		const float fAORadius = 0.5; 
		vec3 v3Offset = m3TangentAxis * v3Samples[i] * fAORadius; 
		vec3 v3OffsetPos = v3Position + v3Offset; 
		// Compute screen space coordinates 
		vec4 v4OffsetPos = m4ViewProjection * vec4( v3OffsetPos, 1.0 ); 
		v3OffsetPos = v4OffsetPos.xyz / v4OffsetPos.w; 
		v3OffsetPos = ( v3OffsetPos * 0.5 ) + 0.5; 
		// Read depth buffer 
		float fSampleDepth = texture( s2DepthTexture, v3OffsetPos.xy ).r; 
		// Compare sample depth with depth buffer value 
		const float fEpsilon = 0.00000001; 
		float fRangeCheck = ( abs( v3OffsetPos.z - fSampleDepth ) < fAORadius )? 1.0 : 0.0; 
		fAO += ( fSampleDepth <= v3OffsetPos.z + fEpsilon )? fRangeCheck : 0.0; 
	} 
	fAOOut = 1.0 - ( fAO / 16 );


}