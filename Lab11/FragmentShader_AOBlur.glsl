#version 440 

layout( binding = 7 ) uniform InvResolution { 
	vec2 v2InvResolution; 
}; 

layout( binding = 18 ) uniform sampler2D s2AOTexture; 
out vec3 v3AOOut; 

void main( )
{ 
	// Get UV coordinates 
	vec2 v2UV = gl_FragCoord.xy * v2InvResolution; 
	// Calculate 4x4 blur start and width 
	vec2 v2StartUV = v2UV - ( v2InvResolution * 1.5 ); 
	vec2 v2UVOffset = v2InvResolution * 2.0; 

	// Perform 4x4 blur of AO samples 
	float fAO = texture( s2AOTexture, v2StartUV ).r; 
	fAO += texture( s2AOTexture, v2StartUV + vec2( v2UVOffset.x, 0.0 ) ).r; 
	fAO += texture( s2AOTexture, v2StartUV + vec2( 0.0, v2UVOffset.y ) ).r; 
	fAO += texture( s2AOTexture, v2StartUV + v2UVOffset ).r; 
	fAO *= 0.25; 
	v3AOOut = vec3( fAO ); 
} 