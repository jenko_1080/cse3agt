#version 440
layout( location = 0 ) out float fFragDepth;

void main( )
{
	// Modify Main
	// - Use front face culling
	// - check alpha

	// Not really needed, OpenGL does it anyway
	fFragDepth = gl_FragCoord.z;
}