#version 440
layout( location = 0 ) in vec4 v4Colour;
out vec4 v4ColourOut;

void main() {
	v4ColourOut = v4Colour;
}