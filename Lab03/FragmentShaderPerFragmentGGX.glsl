#version 440
// Defines //
/////////////
#define M_RCPPI 0.31830988618379067153776752674503
#define M_PI 3.1415926535897932384626433832795

// Data Structures //
/////////////////////
struct PointLight { 
	vec3 v3LightPosition; 
	vec4 v4LightIntensity; 
	vec3 v3Falloff; 
};

// Allow up to MAX_LIGHTS light sources
#define MAX_LIGHTS 8
uniform PointLightData {
	PointLight PointLights[MAX_LIGHTS];
};
uniform int iNumPointLights;

uniform MaterialData {
	vec4 v4DiffuseColour;
	vec4 v4SpecularColour;
	float fRoughness;
};
uniform CameraData {
	mat4 m4ViewProjection;
	vec3 v3CameraPosition;
};


// Inputs
layout( location = 0 ) in vec4 v4Position;
layout( location = 1 ) in vec4 v4Normal;
// Outputs
out vec4 v4ColourOut;

// Function Prototypes //
/////////////////////////
subroutine vec3 BRDF( vec3, vec3, vec3, vec3 ); // Allows fast swapping between shaders
subroutine uniform BRDF BRDFUniform;
vec3 lightFalloff( in vec3 v3LightIntensity, in vec3 v3Falloff, in vec3 v3LightPosition, in vec3 v3Position ); // Both
vec3 schlickFresnel( in vec3 v3LightDirection, in vec3 v3HalfVector );	// for GGX
float TRDistribution( in vec3 v3Normal, in vec3 v3HalfVector );			// for GGX
float GGXVisibility( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection );	// for GGX
vec3 blinnPhong( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in vec3 v3LightIrradiance );
vec3 GGX( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in vec3 v3LightIrradiance ); // for GGX

void main() {
	// Normalize the inputs
	vec3 v3TNormal = normalize( v4Normal.xyz );
	vec3 v3TViewDir = normalize( v3CameraPosition - v4Position.xyz );

	/* new code */
	// Create / Initialise vars for lighting
	//vec3 v3TLightDir = vec3(0.0, 0.0, 0.0);
	//vec3 v3LightIrradiance = vec3(0.0, 0.0, 0.0);
	vec3 v3ColourOut = vec3(0.0, 0.0, 0.0);

	for(int i = 0; i < iNumPointLights; ++i)
	{
		vec3 v3TLightDir = normalize( PointLights[i].v3LightPosition - v4Position.xyz );
		// Calculate light falloff
		vec3 v3LightIrradiance = lightFalloff( PointLights[i].v4LightIntensity.rgb, PointLights[i].v3Falloff, PointLights[i].v3LightPosition, v4Position.xyz );
		// Perform shading and ADD to current output
		v3ColourOut += BRDFUniform( v3TNormal, v3TLightDir, v3TViewDir, v3LightIrradiance );
	}
	// Add Diffuse Lighting
	v3ColourOut += v4DiffuseColour.xyz * vec3( 0.05, 0.05, 0.05 );

	v4ColourOut = vec4( v3ColourOut, 1.0 );
}

// Functions //
///////////////
subroutine(BRDF) vec3 blinnPhong( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in vec3 v3LightIrradiance ) {
	// Convert GGX roughness to Phong shininess
	float fRoughnessPhong = ( 2.0 / ( fRoughness * fRoughness ) ) - 2;
	// Calculate diffuse component
	vec3 v3Diffuse = v4DiffuseColour.rgb;
	// Calculate half vector
	vec3 v3HalfVector = normalize( v3ViewDirection + v3LightDirection );
	// Calculate specular component
	vec3 v3Specular = pow( max( dot( v3Normal, v3HalfVector ), 0.0 ), fRoughnessPhong ) * v4SpecularColour.rgb;
	// Normalize diffuse and specular component
	v3Diffuse *= M_RCPPI;
	v3Specular *= ( fRoughnessPhong + 8 ) / ( 8 * M_PI );
	//Combine diffuse and specular
	vec3 v3RetColour = v3Diffuse + v3Specular;
	// Multiply by view angle
	v3RetColour *= max( dot( v3Normal, v3LightDirection ), 0.0 );
	// Combine with incoming light value 
	v3RetColour *= v3LightIrradiance;
	return v3RetColour;
}

vec3 lightFalloff( in vec3 v3LightIntensity, in vec3 v3Falloff, in vec3 v3LightPosition, in vec3 v3Position )
{
	// Calculate distance from light
	float fDist = distance( v3LightPosition, v3Position );
	// Return fallof
	//return v3LightIntensity / ( fFalloff * fDist * fDist );
	return v3LightIntensity / (( v3Falloff[0] ) + ( v3Falloff[1] * fDist ) + ( v3Falloff[2] * fDist * fDist ));
}

vec3 schlickFresnel( in vec3 v3LightDirection, in vec3 v3HalfVector )
{
	// Schlick Fresnel approximation
	float fLH = dot( v3LightDirection, v3HalfVector );
	return v4SpecularColour.rgb + ( 1.0 - v4SpecularColour.rgb ) * pow( 1.0 - fLH, 5 );
}

float TRDistribution( in vec3 v3Normal, in vec3 v3HalfVector )
{
	// Trowbridge-Reitz Distribution function
	float fNSq = fRoughness * fRoughness;
	float fNH = max( dot( v3Normal, v3HalfVector ), 0.0 );
	float fDenom = fNH * fNH * ( fNSq - 1 ) + 1;
	return fNSq / ( M_PI * fDenom * fDenom );
}

float GGXVisibility( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection )
{
	// GGX Visibility function
	float fNL = max( dot( v3Normal, v3LightDirection ), 0.0 );
	float fNV = max( dot( v3Normal, v3ViewDirection ), 0.0 );
	float fRSq = fRoughness * fRoughness;
	float fRMod = 1.0 - fRSq;
	float fRecipG1 = fNL + sqrt( fRSq + ( fRMod * fNL * fNL ) );
	float fRecipG2 = fNV + sqrt( fRSq + ( fRMod * fNV * fNV ) );
	return 1.0 / ( fRecipG1 * fRecipG2 );
}

subroutine(BRDF) vec3 GGX( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in vec3 v3LightIrradiance )
{
	// Calculate half vector
	vec3 v3HalfVector = normalize( v3ViewDirection + v3LightDirection );
	
	// Calculate diffuse component
	vec3 v3Diffuse = v4DiffuseColour.rgb * M_RCPPI;
	// scale diffuse term
	float Fln = max(dot( v3LightDirection, v3HalfVector), 0.0); // using half vector instead of normal, and max for correction
	v3Diffuse *= 1 - (v4DiffuseColour.xyz * Fln);

	// Calculate Toorance-Sparrow components
	vec3 v3F = schlickFresnel( v3LightDirection, v3HalfVector );
	float fD = TRDistribution( v3Normal, v3HalfVector );
	float fV = GGXVisibility( v3Normal, v3LightDirection, v3ViewDirection );

	//Combine diffuse and specular
	vec3 v3RetColour = v3Diffuse + v3F * fD * fV;

	// Multiply by view angle
	v3RetColour *= max( dot( v3Normal, v3LightDirection ), 0.0 );
	// Combine with incoming light value
	v3RetColour *= v3LightIrradiance;

	return v3RetColour;
}