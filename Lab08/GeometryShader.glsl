#version 440
layout( binding = 4 ) uniform CameraCubeData
{
	mat4 m4ViewProjectionCube[6];
};

layout( triangles, invocations = 6 ) in;
layout( triangle_strip, max_vertices = 3 ) out;

layout( location = 0 ) in vec4 v4VertexPos[];
layout( location = 1 ) in vec4 v4VertexNormal[];
layout( location = 2 ) in vec2 v2VertexUV[];
layout( location = 3 ) in vec4 v4VertexTangent[];

layout( location = 0 ) smooth out vec4 v4Position;
layout( location = 1 ) smooth out vec4 v4Normal;
layout( location = 2 ) smooth out vec2 v2UV;
layout( location = 3 ) smooth out vec4 v4Tangent;

void main() {
	// Loop over each vertex in the face and output
	for( int i = 0; i < 3; ++i )
	{
		// Transform position
		v4Position = v4VertexPos[i]; 
		gl_Position = m4ViewProjectionCube[gl_InvocationID] * v4VertexPos[i];

		// Transform normal
		v4Normal = m4ViewProjectionCube[gl_InvocationID] * v4VertexNormal[i];

		// Transform tangent 
		v4Tangent = m4ViewProjectionCube[gl_InvocationID] * v4VertexTangent[i];

		//Pass-through UV coordinates
		v2UV = v2VertexUV[i];

		// Output to cubemap layer based on invocation ID
		gl_Layer = gl_InvocationID;
		EmitVertex();
	}
	EndPrimitive();
}