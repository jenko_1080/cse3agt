#version 440
#define MAX_LIGHTS 32

layout( binding = 6 ) uniform CameraShadowData
{
	mat4 m4ViewProjectionShadow[MAX_LIGHTS];
};

layout( location = 0 ) uniform int iNumLights;

layout( triangles, invocations = MAX_LIGHTS ) in;
layout( triangle_strip, max_vertices = 3 ) out;

layout( location = 0 ) in vec4 v4VertexPos[];

void main()
{
	// Check if valid invocation
	if( gl_InvocationID < iNumLights )
	{
		// Loop over each vertex in the face and output
		for( int i = 0; i < 3; ++i )
		{
			// Transform Position
			gl_Position = m4ViewProjectionShadow[gl_InvocationID] * v4VertexPos[i];

			// Output to array layer based on invocation ID
			gl_Layer = gl_InvocationID;
			EmitVertex( );
		}
		EndPrimitive( );
	}
}