#version 440
// Defines //
/////////////
#define M_RCPPI 0.31830988618379067153776752674503
#define M_PI 3.1415926535897932384626433832795

// Data Structures //
/////////////////////
layout( binding = 1 ) uniform TransformData {
	mat4 m4Transform;
};
layout( binding = 0 ) uniform CameraData {
	mat4 m4ViewProjection;
	vec3 v3CameraPosition;
};

// Inputs
layout( location = 0 ) in vec4 v4VertexPos;
layout( location = 1 ) in vec4 v4VertexNormal;
layout( location = 2 ) in vec2 v2VertexUV;
layout( location = 3 ) in vec4 v4VertexTangent;
// Outputs
layout( location = 0 ) smooth out vec4 v4Position;
layout( location = 1 ) smooth out vec4 v4Normal;
layout( location = 2 ) smooth out vec2 v2UV;
layout( location = 3 ) smooth out vec4 v4Tangent;

// Function Prototypes //
/////////////////////////

// Main //
//////////
void main() {
	// Transform vertex
	v4Position = m4Transform * v4VertexPos;
	gl_Position = m4ViewProjection * v4Position; // removed for lab6-73
	// Transform Normal
	v4Normal = m4Transform * v4VertexNormal;
	//  Transform the tangent
	v4Tangent = m4Transform * v4VertexTangent;
	// Pass through UV coords
	v2UV = v2VertexUV;
}

// Functions //
///////////////

