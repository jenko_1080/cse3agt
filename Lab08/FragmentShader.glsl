#version 440
// Defines //
/////////////
#define M_RCPPI 0.31830988618379067153776752674503
#define M_PI 3.1415926535897932384626433832795
#define MAX_LIGHTS 16

// Data Structures //
/////////////////////
struct PointLight { 
	vec4 v4LightPosition; 
	vec4 v4LightIntensity; 
	vec3 v3Falloff; 
	vec2 v2NearFar;
};
struct SpotLight {
	vec3 v3LightPosition;
	vec3 v3LightDirection;
	vec4 v4LightIntensity;
	vec3 v3Falloff;
	float fCosAngle;
};

layout( binding = 2 ) uniform PointLightData
{
	PointLight PointLights[MAX_LIGHTS];
};
layout( binding = 5 ) uniform SpotLightData 
{ 
	SpotLight SpotLights[MAX_LIGHTS];
};
layout( binding = 6 ) uniform CameraShadowData
{
	mat4 m4ViewProjectionShadow[MAX_LIGHTS];
};
// Program Inputs
layout( location = 0 ) in vec4 v4Position;
layout( location = 1 ) in vec4 v4Normal;
layout( location = 2 ) in vec2 v2UV;
layout( location = 3 ) in vec4 v4Tangent;
layout( binding = 3 ) uniform ReflectPlaneData // UBO for data required to map the texture to the planes surface
{
	mat4 m4ReflectVP;
};

// Program Outputs
out vec4 v4ColourOut;


layout( location = 0 ) uniform int iNumPointLights;
layout( location = 1 ) uniform float fEmissivePower;
layout( location = 2 ) uniform int iNumSpotLights;

// Add in uniforms for textures (Layouts for easy init in main program)
layout( binding = 0 ) uniform sampler2D s2DiffuseTexture; 
layout( binding = 1 ) uniform sampler2D s2SpecularTexture; 
layout( binding = 2 ) uniform sampler2D s2RoughnessTexture;
layout( binding = 3 ) uniform samplerCube scRefractMapTexture; // Refraction / Transparency cubemap
layout( binding = 4 ) uniform sampler2D s2ReflectTexture; // Create additional texture input for object's reflection texture
layout( binding = 5 ) uniform samplerCube scReflectMapTexture; // Create additional texture input for object's cubemap reflection texture
layout( binding = 6 ) uniform sampler2DArrayShadow s2aShadowTexture; // Spot light shadows
layout( binding = 7 ) uniform samplerCubeArrayShadow scaPointShadowTexture; // Point light shadows
//layout( binding = 8 ) uniform sampler2D transparentTexture; // TODO: FOR TRANSPARENT MATERIALS
layout( binding = 9 ) uniform sampler2D s2NormalTexture;
layout( binding = 10 ) uniform sampler2D s2BumpTexture;


// Temporary vars
// Size of bump map
const float fBumpScale = 0.01;


layout( binding = 0 ) uniform CameraData
{
	mat4 m4ViewProjection;
	vec3 v3CameraPosition;
};



subroutine vec3 Emissive( vec3, vec4 ); // Set up subroutine for emissive objects
layout( location = 0 ) subroutine uniform Emissive EmissiveUniform; // Set up subroutine for emissive objects
subroutine vec3 RefractMap( vec3, vec3, vec3, vec4, vec4 ); // Set up subroutine for transparent materials
layout( location = 1 ) subroutine uniform RefractMap RefractMapUniform;
subroutine vec3 ReflectMap( vec3, vec3, vec3, vec4, float ); // Set up subroutine for reflective materials
layout( location = 2 ) subroutine uniform ReflectMap ReflectMapUniform;

// Function Prototypes //
/////////////////////////
vec3 lightFalloff( in vec3 v3LightIntensity, in vec3 v3Falloff, in vec3 v3LightPosition, in vec3 v3Position ); // Both
vec3 schlickFresnel( in vec3 v3LightDirection, in vec3 v3HalfVector, in vec4 v4SpecularColour );
float TRDistribution( in vec3 v3Normal, in vec3 v3HalfVector, in float fRoughness );			// for GGX
float GGXVisibility( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in float fRoughness );	// for GGX
vec3 GGX( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in vec3 v3LightIrradiance, in vec4 v4DiffuseColour, in vec4 v4SpecularColour, in float fRoughness ); // for GGX
vec3 SpecularTransmit( in vec3 v3Normal, in vec3 v3ViewDirection, in vec4 v4DiffuseColour, in vec4 v4SpecularColour );
vec3 GGXReflect( in vec3 v3Normal, in vec3 v3ReflectDirection, in vec3 v3ViewDirection, in vec3 v3ReflectRadiance, in vec4 v4SpecularColour, in float fRoughness );
float lightShadow( in int iLight );
float lightPointShadow( in int iLight, in vec3 v3LightDirection, in vec3 v3TLightDirection, in vec2 v2NearFar );
float random( in vec3 v3Seed, in float fFreq );
vec3 normalMap( in vec3 v3Normal, in vec3 v3Tangent, in vec3 v3BiTangent, in vec2 v2LocalUV );
vec2 parallaxMap( in vec3 v3Normal, in vec3 v3Tangent, in vec3 v3BiTangent, in vec3 v3ViewDirection );

// Subroutines //
/////////////////
// Emissive materials
layout( index = 0 ) subroutine(Emissive) vec3 noEmissive( vec3 v3ColourOut, vec4 v4DiffuseColour )
{
	return v3ColourOut; // Return colour unmodified
}
layout( index = 1 ) subroutine(Emissive) vec3 textureEmissive( vec3 v3ColourOut, vec4 v4DiffuseColour )
{
	return v3ColourOut + ( fEmissivePower * v4DiffuseColour.xyz ); // Add in emissive contribution
}
// Transparent materials
layout( index = 2 ) subroutine(RefractMap) vec3 noRefractMap( vec3 v3ColourOut, vec3 v3Normal, vec3 v3ViewDirection, vec4 v4DiffuseColour, vec4 v4SpecularColour )
{
	return v3ColourOut; // Return colour unmodified
}
layout( index = 3 ) subroutine(RefractMap) vec3 textureRefractMap( vec3 v3ColourOut, vec3 v3Normal, vec3 v3ViewDirection, vec4 v4DiffuseColour, vec4 v4SpecularColour )
{
	// Get specular transmittance term
	vec3 v3Transmit = SpecularTransmit( v3Normal, v3ViewDirection, v4DiffuseColour, v4SpecularColour );

	// Add in transparent contribution and blend with existing
	return mix( v3Transmit, v3ColourOut, v4DiffuseColour.w );
}

// Reflections, planar surfaces get texture reflection - curved get a cubemap
layout( index = 4 ) subroutine(ReflectMap) vec3 noReflectMap( vec3 v3ColourOut, vec3 v3Normal, vec3 v3ViewDirection, vec4 v4SpecularColour, float fRoughness )
{
	return v3ColourOut; // Return colour unmodified
}
layout( index = 5 ) subroutine(ReflectMap) vec3 textureReflectPlane( vec3 v3ColourOut, vec3 v3Normal, vec3 v3ViewDirection, vec4 v4SpecularColour, float fRoughness )
{
	// UV calculations
	vec4 v4RVPPosition = m4ReflectVP * v4Position;
	vec2 v2ReflectUV = v4RVPPosition.xy / v4RVPPosition.w;
	v2ReflectUV = ( v2ReflectUV + 1.0 ) * 0.50;

	// LOD offset and texture lookup
	// Calculate LOD offset
	float fLOD = textureQueryLod( s2ReflectTexture, v2ReflectUV ).y;
	float fGloss = 1.0 - fRoughness;
	fLOD += ( ( 2.0 / ( fGloss * fGloss ) ) - 1.0 );

	vec3 v3ReflectRadiance = textureLod( s2ReflectTexture, v2ReflectUV, fLOD ).xyz;
	
	// Reflection direction calculation
	vec3 v3ReflectDirection = normalize( reflect( -v3ViewDirection, v3Normal ) );

	// Shading
	vec3 v3RetColour = GGXReflect( v3Normal, v3ReflectDirection, v3ViewDirection, v3ReflectRadiance, v4SpecularColour, fRoughness );
	
	return v3ColourOut + v3RetColour;
}
layout( index = 6 ) subroutine(ReflectMap) vec3 textureReflectCube( vec3 v3ColourOut, vec3 v3Normal, vec3 v3ViewDirection, vec4 v4SpecularColour, float fRoughness )
{
	// Get reflect direction
	vec3 v3ReflectDirection = normalize( reflect( -v3ViewDirection, v3Normal ) );

	// Calculate LOD offset
	float fLOD = textureQueryLod( scReflectMapTexture, v3ReflectDirection ).y;
	float fGloss = 1.0 - fRoughness;
	fLOD += ( ( 2.0 / ( fGloss * fGloss ) ) - 1.0 );

	// Get reflect texture data
	vec3 v3ReflectRadiance = textureLod( scReflectMapTexture, v3ReflectDirection, fLOD ).xyz;

	// Perform shading
	vec3 v3RetColour = GGXReflect( v3Normal, v3ReflectDirection, v3ViewDirection, v3ReflectRadiance, v4SpecularColour, fRoughness );

	return v3ColourOut + v3RetColour;
}

// Main //
//////////
void main() {
	// Set up view direction
	vec3 v3TViewDirection = normalize( v3CameraPosition - v4Position.xyz );
	
	// Normalize the inputs
	vec3 v3TNormal = normalize( v4Normal.xyz );
	vec3 v3TTangent = normalize( v4Tangent.xyz );
	// Generate bitangent 
	vec3 v3BiTangent = cross( v3TNormal, v3TTangent );
	// Perform Parallax Occlusion Mapping 
	vec2 v2UVPO = parallaxMap( v3TNormal, v3TTangent, v3BiTangent, v3TViewDirection );
	// Perform Bump Mapping (now using parallax occlusion map)
	v3TNormal = normalMap( v3TNormal, v3TTangent, v3BiTangent, v2UVPO );

	// Get texture data
	vec4 v4DiffuseColour = texture( s2DiffuseTexture, v2UVPO );
	vec4 v4SpecularColour = texture( s2SpecularTexture, v2UVPO );
	float fRoughness = texture( s2RoughnessTexture, v2UVPO ).r;

	// Create / Initialise vars for lighting
	vec3 v3ColourOut = vec3(0.0, 0.0, 0.0);

	// Loop over each point light
	for(int i = 0; i < iNumPointLights; ++i)
	{
		// Normalize light direction
		vec3 v3TLightDir = normalize( PointLights[i].v4LightPosition.xyz - v4Position.xyz );
		// Calculate light falloff
		vec3 v3LightIrradiance = lightFalloff( PointLights[i].v4LightIntensity.rgb, PointLights[i].v3Falloff, PointLights[i].v4LightPosition.xyz, v4Position.xyz );
		// Calculate shadowing
		float fShadowing = lightPointShadow( i, PointLights[i].v4LightPosition.xyz - v4Position.xyz, normalize( PointLights[i].v4LightPosition.xyz - v4Position.xyz ), PointLights[i].v2NearFar );
		v3LightIrradiance *= fShadowing; // if area is in shadow, multiplied by 0... if not 1

		// Perform shading and ADD to current output
		v3ColourOut += GGX( v3TNormal, v3TLightDir, v3TViewDirection, v3LightIrradiance, v4DiffuseColour, v4SpecularColour, fRoughness );
		//v3ColourOut += vec3(1.0,1.0,1.0) * fShadowing;
	}

	// Loop over each spot light
	for( int i = 0; i < iNumSpotLights; ++i )
	{
		vec3 v3TLightDirection = normalize( SpotLights[i].v3LightPosition - v4Position.xyz );

		//Check light angle
		float fLightAngle = dot( v3TLightDirection, SpotLights[i].v3LightDirection );
		if( fLightAngle >= SpotLights[i].fCosAngle )
		{
			//Calculate light falloff
			vec3 v3LightIrradiance = lightFalloff( SpotLights[i].v4LightIntensity.xyz, SpotLights[i].v3Falloff, SpotLights[i].v3LightPosition, v4Position.xyz );

			// Calculate shadowing
			float fShadowing = lightShadow( i );
			v3LightIrradiance *= fShadowing; // if area is in shadow, multiplied by 0... if not 1

			// Perform shading
			v3ColourOut += GGX( v3TNormal, v3TLightDirection, v3TViewDirection, v3LightIrradiance, v4DiffuseColour, v4SpecularColour, fRoughness );
		}
	}

	// Add Diffuse Lighting
	v3ColourOut += v4DiffuseColour.xyz * vec3( 0.14, 0.14, 0.14 );
	// Add in any refraction contribution
	v3ColourOut = RefractMapUniform( v3ColourOut, v3TNormal, v3TViewDirection, v4DiffuseColour, v4SpecularColour );
	// Add in any reflection contribution
	v3ColourOut = ReflectMapUniform( v3ColourOut, v3TNormal, v3TViewDirection, v4SpecularColour, fRoughness );
	// Add in any emissive contribution
	v3ColourOut = EmissiveUniform( v3ColourOut, v4DiffuseColour );
	// Output colour information
	//v4ColourOut = vec4( v3TNormal, 1.0);
	v4ColourOut = vec4( v3ColourOut, 1.0 );
}

// Functions //
///////////////
vec3 lightFalloff( in vec3 v3LightIntensity, in vec3 v3Falloff, in vec3 v3LightPosition, in vec3 v3Position )
{
	// Calculate distance from light
	float fDist = distance( v3LightPosition, v3Position );
	// Return fallof
	//return v3LightIntensity / ( fFalloff * fDist * fDist );
	return v3LightIntensity / (( v3Falloff[0] ) + ( v3Falloff[1] * fDist ) + ( v3Falloff[2] * fDist * fDist ));
}

vec3 schlickFresnel( in vec3 v3LightDirection, in vec3 v3HalfVector, in vec4 v4SpecularColour )
{
	// Schlick Fresnel approximation
	float fLH = dot( v3LightDirection, v3HalfVector );
	return v4SpecularColour.rgb + ( 1.0 - v4SpecularColour.rgb ) * pow( 1.0 - fLH, 5 );
}

float TRDistribution( in vec3 v3Normal, in vec3 v3HalfVector, in float fRoughness )
{
	// Trowbridge-Reitz Distribution function
	float fNSq = fRoughness * fRoughness;
	float fNH = max( dot( v3Normal, v3HalfVector ), 0.0 );
	float fDenom = fNH * fNH * ( fNSq - 1 ) + 1;
	return fNSq / ( M_PI * fDenom * fDenom );
}

float GGXVisibility( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in float fRoughness )
{
	// GGX Visibility function
	float fNL = max( dot( v3Normal, v3LightDirection ), 0.0 );
	float fNV = max( dot( v3Normal, v3ViewDirection ), 0.0 );
	float fRSq = fRoughness * fRoughness;
	float fRMod = 1.0 - fRSq;
	float fRecipG1 = fNL + sqrt( fRSq + ( fRMod * fNL * fNL ) );
	float fRecipG2 = fNV + sqrt( fRSq + ( fRMod * fNV * fNV ) );
	return 1.0 / ( fRecipG1 * fRecipG2 );
}

vec3 GGX( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in vec3 v3LightIrradiance, in vec4 v4DiffuseColour, in vec4 v4SpecularColour, in float fRoughness )
{
	// Calculate half vector
	vec3 v3HalfVector = normalize( v3ViewDirection + v3LightDirection );
	
	// Calculate diffuse component
	vec3 v3Diffuse = v4DiffuseColour.rgb * M_RCPPI;
	// scale diffuse term
	float Fln = max(dot( v3LightDirection, v3HalfVector), 0.0); // using half vector instead of normal, and max for correction
	v3Diffuse *= 1 - (v4DiffuseColour.xyz * Fln);

	// Calculate Toorance-Sparrow components
	vec3 v3F = schlickFresnel( v3LightDirection, v3HalfVector, v4SpecularColour );
	float fD = TRDistribution( v3Normal, v3HalfVector, fRoughness );
	float fV = GGXVisibility( v3Normal, v3LightDirection, v3ViewDirection, fRoughness );

	//Combine diffuse and specular
	vec3 v3RetColour = v3Diffuse + v3F * fD * fV;

	// Multiply by view angle
	v3RetColour *= max( dot( v3Normal, v3LightDirection ), 0.0 );
	// Combine with incoming light value
	v3RetColour *= v3LightIrradiance;

	return v3RetColour;
}

vec3 SpecularTransmit( in vec3 v3Normal, in vec3 v3ViewDirection, in vec4 v4DiffuseColour, in vec4 v4SpecularColour )
{
	// Calculate index of refraction from Fresnel term
	float fRootF0 = sqrt( v4SpecularColour.x );
	float fIOR = ( 1 - fRootF0 ) / ( 1 + fRootF0 );
	// Get refraction direction 
	vec3 v3Refract = refract( -v3ViewDirection, v3Normal, fIOR );
	// Get refraction map data 
	vec3 v3RefractColour = texture( scRefractMapTexture, v3Refract ).xyz;
	
	// Evaluate specular transmittance 
	vec3 v3RetColour = fIOR * ( 1.0 - schlickFresnel( v3Refract, -v3Normal, v4SpecularColour ) );
	v3RetColour *= v4DiffuseColour.xyz;
	// Combine with incoming light value 
	v3RetColour *= v3RefractColour;
	return v3RetColour; 
}

vec3 GGXReflect( in vec3 v3Normal, in vec3 v3ReflectDirection, in vec3 v3ViewDirection, in vec3 v3ReflectRadiance, in vec4 v4SpecularColour, in float fRoughness )
{
	// Calculate Torrence-Sparrow components
	vec3 v3F = schlickFresnel( v3ReflectDirection, v3Normal, v4SpecularColour );
	float fV = GGXVisibility( v3Normal, v3ReflectDirection, v3ViewDirection, fRoughness );

	// Combine specular
	vec3 v3RetColour = v3F * fV;

	// Modify by pdf
	v3RetColour *= ( 4 * dot( v3ViewDirection, v3Normal ) );

	// Multiply by view angle
	v3RetColour *= max( dot( v3Normal, v3ReflectDirection ), 0.0 );

	// Combine with incoming light value
	v3RetColour *= v3ReflectRadiance;	

	return v3RetColour;
}

// For spot lights
float lightShadow( in int iLight )
{
	// Get position in shadow texture
	vec4 v4SVPPosition = m4ViewProjectionShadow[iLight] * v4Position;
	vec3 v3SVPPosition = v4SVPPosition.xyz / v4SVPPosition.w;
	v3SVPPosition = ( v3SVPPosition + 1.0 ) * 0.5;

	// Define poisson disk sampling values
	const vec2 v2PoissonDisk[9] = vec2[](
		vec2( -0.01529481, -0.07395129 ),
		vec2( -0.56232890, -0.36484920 ),
		vec2(  0.95519960,  0.18418130 ),
		vec2(  0.20716880,  0.49262790 ),
		vec2( -0.01290792, -0.95755550 ),
		vec2(  0.68047200, -0.51716110 ),
		vec2( -0.60139470,  0.37665210 ),
		vec2( -0.40243310,  0.86631060 ),
		vec2( -0.96646290, -0.04688413 )
	);

	// Get Texture dimensions
	//int iTextSize = textureSize( s2aShadowTexture, 0 ).x;
	// Light perspective space size of lights area on near plane 
	const float fLightSize = 0.07;
	// Approximate near plane size of light
	float fShadowRegion = fLightSize * v3SVPPosition.z; 
	float fShadowSize = fShadowRegion / 9;

	// Generate random rotation
	float fAngle = random( v4Position.xyz, 500 ) * ( M_PI * 2 );
	vec2 v2Rotate = vec2( sin( fAngle ), cos( fAngle ) );

	// Perform additional filtering
	float fShadowing = 0.0;
	for( int i = 0 ; i <= 9 ; i++ )
	{
		vec2 v2RotatedPoisson = ( v2PoissonDisk[i].x * v2Rotate.yx ) + ( v2PoissonDisk[i].y * v2Rotate * vec2( -1, 1 ) );
		vec2 v2Offset = v2RotatedPoisson * fShadowSize;
		vec3 v3UVC = v3SVPPosition + vec3( v2Offset, 0.0 );
		float fText=texture( s2aShadowTexture, vec4( v3UVC.xy, iLight, v3UVC.z ));
		fShadowing += fText;
	}

	return fShadowing / 9;
}

// for Point Lights
float lightPointShadow( in int iLight, in vec3 v3LightDirection, in vec3 v3TLightDirection, in vec2 v2NearFar )
{
	// Get depth in shadow texture
	vec3 v3AbsDirect = abs( v3LightDirection );
	float fDist = max( v3AbsDirect.x, max( v3AbsDirect.y, v3AbsDirect.z ) );
	float fDepth = v2NearFar.y + v2NearFar.x;
	fDepth += ( -2 * v2NearFar.y * v2NearFar.x ) / fDist;
	fDepth /= v2NearFar.y - v2NearFar.x;
	fDepth = ( fDepth + 1 ) * 0.5;

	// Define poisson sampling values
	const vec3 v3PoissonDisk[9] = vec3[](
		vec3( -0.023860920, -0.115901396,  0.985948205 ),
		vec3( -0.649357200, -0.542242587,  0.066411376 ),
		vec3(  0.956068397,  0.285292149, -0.865215898 ),
		vec3(  0.228669465,  0.698871136,  0.355417848 ),
		vec3( -0.001350721, -0.997778296, -0.866783142 ),
		vec3(  0.602961421, -0.725908756, -0.338202178 ),
		vec3( -0.672571659,  0.557726085, -0.027191758 ),
		vec3( -0.123172671,  0.978031158, -0.663645744 ),
		vec3( -0.995905936, -0.073578961, -0.894974828 )
	);

	// Get Texture dimensions
	//int iTextSize = textureSize( s2aShadowTexture, 0 ).x;
	//float fTexOffset = 1.0 / iTextSize;
	// Light perspective space size of lights area on near plane 
	const float fLightSize = 0.07;
	// Approximate near plane size of light
	float fShadowRegion = fLightSize * fDepth; 
	float fShadowSize = fShadowRegion / 9;

	// Generate random rotation
	float fAngle = random( v4Position.xyz, 500 ) * ( M_PI * 2 );
	vec3 v3Rotate = vec3( sin( fAngle ), cos( fAngle ), sin( fAngle ) );

	// Perform additional filtering
	float fShadowing = 0.0;
	for( int i = 0 ; i <= 9 ; i++ )
	{
		vec3 v3RotatedPoisson = ( v3PoissonDisk[i].x * v3Rotate.yyz * v3Rotate.zxx ) + ( v3PoissonDisk[i].y * v3Rotate.xyx * v3Rotate.zyy * vec3( -1, 1, 1 ) + ( v3PoissonDisk[i].z * v3Rotate.zxy * vec3( 0, -1, 1 ) ) );
		vec3 v3Offset = v3RotatedPoisson * fShadowSize;
		vec3 v3UVC = -v3TLightDirection + v3Offset;
		float fText = texture( scaPointShadowTexture, vec4( v3UVC, iLight ), fDepth );
		fShadowing += fText;
	}

	return fShadowing / 9;
}

// Random value generator
float random( in vec3 v3Seed, in float fFreq )
{
	// Project seed on random constant vector
	float fdt = dot( floor( v3Seed * fFreq ), vec3( 53.1215, 21.1352, 9.1322 ) );
	// Return only fractional part (range 0->1)
	return fract( sin(fdt) * 2105.2354 );
}

vec3 normalMap( in vec3 v3Normal, in vec3 v3Tangent, in vec3 v3BiTangent, in vec2 v2LocalUV )
{
	// Get normal map value
	vec2 v2NormalMap = ( texture( s2NormalTexture, v2LocalUV ).rg - 0.5 ) * 2;
	vec3 v3NormalMap = vec3( v2NormalMap, sqrt( 1 - length( v2NormalMap ) ) );
	// Convert from tangent space
	vec3 v3RetNormal = mat3( v3Tangent.xy, v3Tangent.z, v3BiTangent.xy, v3BiTangent.z, v3Normal.xy, v3Normal.z ) * v3NormalMap;
	return normalize( v3RetNormal );
}

vec2 parallaxMap( in vec3 v3Normal, in vec3 v3Tangent, in vec3 v3BiTangent, in vec3 v3ViewDirection )
{
	// Get tangent space view direction
	vec3 v3TangentView = vec3( dot( v3ViewDirection, v3Tangent ), dot( v3ViewDirection, v3BiTangent ), dot( v3ViewDirection, v3Normal ) );
	v3TangentView = normalize( v3TangentView );
	// Get number of layers based on view direction
	const float fMinLayers = 5; 
	const float fMaxLayers = 15;
	float fNumLayers = round(mix(fMaxLayers,fMinLayers,abs(v3TangentView.z)));

	// Determine layer height
	float fLayerHeight = 1.0 / fNumLayers;
	// Determine texture offset per layer
	vec2 v2DTex = fBumpScale* v3TangentView.xy / (v3TangentView.z*fNumLayers);
	
	// Get texture gradients to allow for dynamic branching
	vec2 v2Dx = dFdx( v2UV );
	vec2 v2Dy = dFdy( v2UV );

	// Offset start position by half number of layers
	//vec2 v2CurrUV = v2UV + ( v2DTex * fNumLayers * 0.5 );
	vec2 v2CurrUV = v2UV; // Performance increase

	// Initialise height from texture
	float fCurrHeight = textureGrad( s2BumpTexture, v2CurrUV, v2Dx, v2Dy ).r;

	// Loop over each step until lower height is found
	float fViewHeight = 1.0;
	float fLastHeight;
	vec2 v2LastUV;
	for( int i = 0; i < int( fNumLayers ); i++ )
	{
		if( fCurrHeight >= fViewHeight )
			break;

		// Set current values as previous
		fLastHeight = fCurrHeight;
		v2LastUV = v2CurrUV;
		// Go to next layer
		fViewHeight -= fLayerHeight;
		// Shift UV coordinates
		v2CurrUV -= v2DTex;
		// Get new texture height
		fCurrHeight = textureGrad( s2BumpTexture, v2CurrUV, v2Dx, v2Dy ).r;
	}

	// Get heights for linear interpolation
	float fNextHeight = fCurrHeight - fViewHeight;
	float fPrevHeight = fLastHeight - ( fViewHeight + fLayerHeight );

	// Interpolate based on height difference
	float fWeight = fNextHeight / ( fNextHeight - fPrevHeight );
	return mix( v2CurrUV, v2LastUV, fWeight);
}