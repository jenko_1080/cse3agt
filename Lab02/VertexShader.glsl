#version 440
uniform TransformData {
	mat4 m4Transform;
};
uniform CameraData {
	mat4 m4ViewProjection;
};
layout( location = 0 ) in vec4 v4VertexPos;
void main() {
	gl_Position = m4ViewProjection * ( m4Transform * v4VertexPos );
}