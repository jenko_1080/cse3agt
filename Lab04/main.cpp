// Using SDL, SDL OpenGL, GLEW
#define GLEW_STATIC
#include <GL/glew.h>
#include <SDL.h>
#include <SDL_opengl.h>
// GLM and math headers (lab02)
#include <math.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
// Using KTX import 
#define KTX_OPENGL 1 
#include <ktx.h>
bool g_bLoadKTX = true; // Var to tell the program to load a KTX instead of BMP

GLuint g_uiVAO[3];
GLuint g_uiVBO[3];
GLuint g_uiIBO[3];
GLsizei g_iSphereElements;
GLsizei g_iPyramidElements;
GLuint g_uiProgram[3];
glm::mat4 g_m4Transform[5];
GLuint g_uiTransformUBO[5];
GLuint g_uiSubRoutines[2]; // Storage for subroutines

// Vars to load shader
HINSTANCE hInst;
HRSRC hRes;
HGLOBAL hMem;
char * eShader;

// Vars for rotation
float g_objectRotation; // In degrees (0 to 360)
float degreesPerSecond = 1.00f;

// Struct for custom vertex
struct CustomVertex
{
	glm::vec4 v4Position;
	glm::vec4 v4Normal;
	glm::vec2 v2UV; // for texture information
};

// Struct for Camera definition
struct LocalCameraData
{
	float m_fAngleX;
	float m_fAngleY;
	float m_fMoveZ;	// Lab2.33
	float m_fMoveX; // Lab2.33
	glm::vec3 m_v3Position;
	glm::vec3 m_v3Direction;
	glm::vec3 m_v3Right;
	float m_fFOV;
	float m_fAspect;
	float m_fNear;
	float m_fFar;
};
// Struct to pass camera data
struct CameraData
{
	glm::mat4 m_m4ViewProjection;
	glm::vec4 m_v4Position;
};

// Point Light (radiates all directions)
// Position, Colour, Falloff
struct PointLightData
{
	glm::vec4 m_v4Position;
	glm::vec4 m_v4Colour;
	glm::vec4 m_fFalloff; // Changed for memory alignment? (thanks Tom S) (vec3 would also work, glm fixes it)
};

// Texture (was Material) Data
GLuint g_uiTextures[9];

LocalCameraData g_CameraData;
GLuint g_uiCameraUBO;
GLuint g_uiPointLightUBO;

bool GL_Init( int iWindowWidth, int iWindowHeight);
bool GL_LoadShaderResource(GLuint& uiVertexShader, int resourceNumber, int shaderType);
bool GL_LoadShader( GLuint & uiShader, GLenum ShaderType, const GLchar * p_cShader );
bool GL_LoadShaders( GLuint & uiShader, GLuint uiVertexShader, GLuint uiFragmentShader );
bool GL_LoadTextureBMP( GLuint uiTexture, const char * p_cTextureFile );
bool GL_LoadTextureKTX(GLuint uiTexture, const char * p_cTextureFile);
void GL_Update(float fElapsedTime);
void GL_Quit( );
void GL_Render( );
GLsizei GL_GenerateCube(GLuint uiVBO, GLuint uiIBO);
GLsizei GL_GeneratePyramid(GLuint uiVBO, GLuint uiIBO);
GLsizei GL_GenerateSphere( unsigned int uiTessU, unsigned int uiTessV, GLuint uiVBO, GLuint uiIBO);

#ifdef _WIN32
int WINAPI WinMain( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, 
				    _In_ LPSTR lpCmdLine, _In_ int )
#else
int main( int argc, char **argv )
#endif
{
	if( SDL_Init( SDL_INIT_VIDEO ) != 0 )
	{
		SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION,
		"Failed to initialize SDL: %s\n", SDL_GetError() );
		return 1;
	}

	// Use OpenGL 4.4 core profile
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 4 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 4 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
	// Turn on double buffering 24bit Z buffer
	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
	SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 24 );

	// Create a SDL window
	const int iWindowWidth = 1280;
	const int iWindowHeight = 1024;
	const bool bWindowFullscreen = false;

	// Get Desktop Resolution
	SDL_DisplayMode CurrentDisplay;
	SDL_GetCurrentDisplayMode(0, &CurrentDisplay);

	SDL_Window * Window = SDL_CreateWindow( "AGT Tutorial",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, bWindowFullscreen ? CurrentDisplay.w : iWindowWidth, bWindowFullscreen ? CurrentDisplay.h : iWindowHeight,
		SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | (bWindowFullscreen * SDL_WINDOW_FULLSCREEN));
	if( Window == NULL )
	{
		SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION, "Failed to create OpenGL window: %s\n", SDL_GetError() );
		SDL_Quit();
		return 1;
	}

	// Create OpenGL Context
	SDL_GLContext Context = SDL_GL_CreateContext( Window );
	if( Context == NULL )
	{
		SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION, "Failed to create OpenGL context: %s\n", SDL_GetError() );
		SDL_DestroyWindow(Window);
		SDL_Quit();
		return 1;
	}

	// VSync (-1 = enable late swaps)
	SDL_GL_SetSwapInterval( -1 );

	// Initialize OpenGL
	if( GL_Init( iWindowWidth, iWindowHeight) ) // User made functions
	{
		//Initialise elapsed time
		Uint32 uiOldTime, uiCurrentTime;
		uiCurrentTime = SDL_GetTicks();

		// Start the program message pump (giggity)
		SDL_Event Event;
		bool bQuit = false;
		while( !bQuit )
		{
			// Update elapsed frame time (for justice and glory)
			uiOldTime = uiCurrentTime;
			uiCurrentTime = SDL_GetTicks();
			float fElapsedTime = (float)(uiCurrentTime - uiOldTime) / 1000.0f;

			// Poll SDL for buffered events
			while( SDL_PollEvent( &Event ) )
			{
				if( Event.type == SDL_QUIT )
					bQuit = true;
				else if ((Event.type == SDL_KEYDOWN) &&
					(Event.key.repeat == 0))
				{
					// Check for escape... derp
					if (Event.key.keysym.sym == SDLK_ESCAPE)
						bQuit = true;
					// Update camera movement vector
					else if (Event.key.keysym.sym == SDLK_w)
						g_CameraData.m_fMoveZ += 4.0f;
					else if (Event.key.keysym.sym == SDLK_a)
						g_CameraData.m_fMoveX -= 4.0f;
					else if (Event.key.keysym.sym == SDLK_s)
						g_CameraData.m_fMoveZ -= 4.0f;
					else if (Event.key.keysym.sym == SDLK_d)
						g_CameraData.m_fMoveX += 4.0f;
					// Update texture filtering
					else if (Event.key.keysym.sym == SDLK_1)
					{
						// No filtering
						for (int i = 0; i < 9; i++)
						{
							glBindTexture(GL_TEXTURE_2D, g_uiTextures[i]);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
						}
					}
					else if (Event.key.keysym.sym == SDLK_2)
					{
						// BiLinear
						for (int i = 0; i < 9; i++)
						{
							glBindTexture(GL_TEXTURE_2D, g_uiTextures[i]);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
						}
					}
					else if (Event.key.keysym.sym == SDLK_3)
					{
						// Mipmap
						for (int i = 0; i < 9; i++)
						{
							glBindTexture(GL_TEXTURE_2D, g_uiTextures[i]);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_NEAREST);
						}
					}
					else if (Event.key.keysym.sym == SDLK_4)
					{
						// TriLinear - Mipmap
						for (int i = 0; i < 9; i++)
						{
							glBindTexture(GL_TEXTURE_2D, g_uiTextures[i]);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
						}
					}
					else if (Event.key.keysym.sym == SDLK_5)
					{
						// TriLinear - Mipmap >> then Anisotropic
						for (int i = 0; i < 9; i++)
						{
							glBindTexture(GL_TEXTURE_2D, g_uiTextures[i]);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
							glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 2);
						}
					}
					else if (Event.key.keysym.sym == SDLK_6)
					{
						// TriLinear - Mipmap >> then Anisotropic
						for (int i = 0; i < 9; i++)
						{
							glBindTexture(GL_TEXTURE_2D, g_uiTextures[i]);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
							glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 4);
						}
					}
				}
				else if (Event.type == SDL_KEYUP)
				{
					// Reset camera movement vector for justice and glory
					if (Event.key.keysym.sym == SDLK_w)
						g_CameraData.m_fMoveZ -= 4.0f;
					else if (Event.key.keysym.sym == SDLK_a)
						g_CameraData.m_fMoveX += 4.0f;
					else if (Event.key.keysym.sym == SDLK_s)
						g_CameraData.m_fMoveZ += 4.0f;
					else if (Event.key.keysym.sym == SDLK_d)
						g_CameraData.m_fMoveX -= 4.0f;
				}
				else if (Event.type == SDL_MOUSEMOTION)
				{
					// Update camera view angles
					g_CameraData.m_fAngleX += -0.1f * fElapsedTime * Event.motion.xrel;
					// Y Coordinates are in screen space so	don't get negated
					g_CameraData.m_fAngleY += 0.1f * fElapsedTime * Event.motion.yrel;

					// Limit camera to 140 degree total rotation... but not using % because it's a float :(
					if (g_CameraData.m_fAngleY > 1.5f)
						g_CameraData.m_fAngleY = 1.5f;
					else if (g_CameraData.m_fAngleY < -1.5f)
						g_CameraData.m_fAngleY = -1.5f;
				}
				else if (Event.type == SDL_MOUSEWHEEL)
				{
					// Calculate new FOV
					float newDeg = glm::degrees(g_CameraData.m_fFOV) + Event.wheel.y;
					// If its within bounds, apply it
					if (newDeg > 9 && newDeg < 150)
					{
						g_CameraData.m_fFOV = glm::radians(newDeg);
					}
				}
				else if (Event.type = SDL_KEYDOWN)
				{
					if (Event.key.keysym.sym == SDLK_ESCAPE)
					{
						bQuit = true;
					}
				}
			}

			// Update the scene (avast!)
			GL_Update(fElapsedTime);
		
			// Render the scene
			GL_Render( );

			// Swap the back-buffer and present
			SDL_GL_SwapWindow( Window );
		}

		GL_Quit(); // Delete GL Resources
	}
	return 0;
}

bool GL_Init( int iWindowWidth, int iWindowHeight)
{
	// Allow experimental / pre-release extensions
	glewExperimental = GL_TRUE;
	GLenum GlewError = glewInit( );
	if( GlewError != GLEW_OK )
	{
		SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION, "Failed to initialize GLEW: %s\n", glewGetErrorString( GlewError) );
		return false;
	}

	// Set up initial GL attributes
	glClearColor( 0.0f, 0.0f, 1.0f, 1.0f);
	glCullFace( GL_BACK );
	glEnable( GL_CULL_FACE );
	glEnable( GL_DEPTH_TEST );
	glDisable( GL_STENCIL_TEST );

	// Create initial model transforms
	// Updated for Lab02.21
	g_m4Transform[0] = glm::mat4( 1.0f ); //Identity matrix
	g_m4Transform[1] = glm::translate( glm::mat4( 1.0f ),glm::vec3( -3.0f, 0.0f, 0.0f ) );
	g_m4Transform[2] = glm::translate( glm::mat4( 1.0f ),glm::vec3( 0.0f, -3.0f, 0.0f ) );
	g_m4Transform[3] = glm::translate( glm::mat4( 1.0f ),glm::vec3( 3.0f, 0.0f, 0.0f ) );
	g_m4Transform[4] = glm::translate( glm::mat4( 1.0f ),glm::vec3( 0.0f, 3.0f, 0.0f ) );

	// Initialise camera data (Lab02.25)
	g_CameraData.m_fAngleX = (float)M_PI;
	g_CameraData.m_fAngleY = 0.0f;
	g_CameraData.m_v3Position = glm::vec3( 0.0f, 0.0f, 12.0f );
	g_CameraData.m_v3Direction = glm::vec3( 0.0f, 0.0f, -1.0f );
	g_CameraData.m_v3Right = glm::vec3( 1.0f, 0.0f, 0.0f );
	// Initialise camera projection values (Lab02.25)
	g_CameraData.m_fFOV = glm::radians( 45.0f );
	g_CameraData.m_fAspect = (float)iWindowWidth / (float)iWindowHeight;
	g_CameraData.m_fNear = 0.1f;
	g_CameraData.m_fFar = 100.0f;

	// Create vertex and fragment shader vars
	GLuint uiVertexShader;
	GLuint uiFragmentShader;
	
	// Only loading GGX shader (Lab 04)
	if (!GL_LoadShaderResource(uiVertexShader, 300, GL_VERTEX_SHADER))
		return false;
	if (!GL_LoadShaderResource(uiFragmentShader, 400, GL_FRAGMENT_SHADER))
		return false;

	// Create Program with current loaded shaders
	if( !GL_LoadShaders( g_uiProgram[0], uiVertexShader, uiFragmentShader ) )
		return false;

	// Clean up old shaders (free mem and invalidate name)
	glDeleteShader( uiVertexShader );
	glDeleteShader( uiFragmentShader );

	//http://stackoverflow.com/a/8923298
	// Create a Vertex Array Object
	glGenVertexArrays( 3, &g_uiVAO[0] );

	// Create Vertex Buffer Object
	glGenBuffers( 3, &g_uiVBO[0] );
	glGenBuffers( 3, &g_uiIBO[0] );

	// Bind the Cube VAO
	glBindVertexArray( g_uiVAO[0] );
	// Create Cube VBO and IBO data
	GL_GenerateCube( g_uiVBO[0], g_uiIBO[0] );
	
	glBindVertexArray( g_uiVAO[1] );
	// Create Sphere VBO and IBO data
	g_iSphereElements = GL_GenerateSphere( 12, 6, g_uiVBO[1], g_uiIBO[1] );
	
	glBindVertexArray( g_uiVAO[2] ); // Bind -THEN- generate
	g_iPyramidElements = GL_GeneratePyramid( g_uiVBO[2], g_uiIBO[2] );

	// Create transform UBOs
	glGenBuffers( 5, &g_uiTransformUBO[0] );
	// Initialise the transform buffers
	for( int i = 0; i < 5; i++ )
	{
		glBindBuffer( GL_UNIFORM_BUFFER, g_uiTransformUBO[i] );
		glBufferData( GL_UNIFORM_BUFFER, sizeof( glm::mat4 ), &g_m4Transform[i], GL_STREAM_DRAW );
	}

	// Create camera UBO (Lab02.26)
	// Camera code moved from here (Lab02.39)
	glGenBuffers(1, &g_uiCameraUBO);
	// Link the uniform buffer used for global transform storage
	uint32_t uiBlockIndex = glGetUniformBlockIndex( g_uiProgram[0], "TransformData" );
	glUniformBlockBinding( g_uiProgram[0], uiBlockIndex, 1 );
	// Link the uniform buffer used for global view projection storage
	uiBlockIndex = glGetUniformBlockIndex( g_uiProgram[0], "CameraData" );
	glUniformBlockBinding( g_uiProgram[0], uiBlockIndex, 0 );
	// Bind camera UBO
	glBindBufferBase( GL_UNIFORM_BUFFER, 0, g_uiCameraUBO );

	// Get light uniform location and set (program 0)
	GLuint uiUIndex = glGetUniformLocation(g_uiProgram[0], "iNumPointLights");
	glProgramUniform1i(g_uiProgram[0], uiUIndex, 3); // set iNumPointLights=3

	// Create point light UBO
	glGenBuffers(1, &g_uiPointLightUBO);
	
	// Lights (3 so far)
	/*
	PointLightData Light[] = {
			{ glm::vec4(9.0f, 9.0f, 9.0f, 1.0f), glm::vec4(1.0f, 0.0f, 0.0f, 1.0f),  glm::vec4(0.000f, 0.000f, 0.005f, 1.0f) },
			{ glm::vec4(-9.0f, 9.0f, 9.0f, 1.0f), glm::vec4(0.0f, 1.0f, 0.0f, 1.0f), glm::vec4(0.000f, 0.000f, 0.005f, 1.0f) },
			{ glm::vec4(0.0f, 9.0f, 0.0f, 1.0f), glm::vec4(0.0f, 1.0f, 1.0f, 1.0f),  glm::vec4(0.000f, 0.000f, 0.005f, 1.0f) }
	};
	*/
	// Lights (3 so far) - ALL White
	PointLightData Light[] = {
			{ glm::vec4(9.0f, 9.0f, 9.0f, 1.0f),  glm::vec4(1.0f, 1.0f, 1.0f, 1.0f), glm::vec4(0.000f, 0.000f, 0.005f, 1.0f) },
			{ glm::vec4(-9.0f, 9.0f, 9.0f, 1.0f), glm::vec4(1.0f, 1.0f, 1.0f, 1.0f), glm::vec4(0.000f, 0.000f, 0.005f, 1.0f) },
			{ glm::vec4(0.0f, 9.0f, 0.0f, 1.0f),  glm::vec4(1.0f, 1.0f, 1.0f, 1.0f), glm::vec4(0.000f, 0.000f, 0.005f, 1.0f) }
	};

	glBindBuffer(GL_UNIFORM_BUFFER, g_uiPointLightUBO);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(PointLightData) * 3, Light, GL_STREAM_DRAW);
	// Link the uniform buffer used for point light storage
	uiBlockIndex = glGetUniformBlockIndex(g_uiProgram[0], "PointLightData");
	glUniformBlockBinding(g_uiProgram[0], uiBlockIndex, 2);
	// Bind Point Light UBO
	glBindBufferBase(GL_UNIFORM_BUFFER, 2, g_uiPointLightUBO);

	// Create texture uniforms (replaces material)
	glGenTextures(9, &g_uiTextures[0]);
	// Check for BMP or KTX option
	if (g_bLoadKTX)
	{
		// Load texture data (Cube)
		if (!GL_LoadTextureKTX(g_uiTextures[0], "../resources/Lab04/CrateDiffuse.ktx"))
			return false;
		if (!GL_LoadTextureKTX(g_uiTextures[1], "../resources/Lab04/CrateSpecular.ktx"))
			return false;
		if (!GL_LoadTextureKTX(g_uiTextures[2], "../resources/Lab04/CrateRoughness.ktx"))
			return false;
		// Load texture data (Sphere)
		if (!GL_LoadTextureKTX(g_uiTextures[3], "../resources/Lab04/WorldDiffuse.ktx"))
			return false;
		if (!GL_LoadTextureKTX(g_uiTextures[4], "../resources/Lab04/WorldSpecular.ktx"))
			return false;
		if (!GL_LoadTextureKTX(g_uiTextures[5], "../resources/Lab04/WorldRoughness.ktx"))
			return false;
		// Load texture data (Pyramid)
		if (!GL_LoadTextureKTX(g_uiTextures[6], "../resources/Lab04/PyramidDiffuse.ktx"))
			return false;
		if (!GL_LoadTextureKTX(g_uiTextures[7], "../resources/Lab04/PyramidSpecular.ktx"))
			return false;
		if (!GL_LoadTextureKTX(g_uiTextures[8], "../resources/Lab04/PyramidRoughness.ktx"))
			return false;
	}
	else
	{
		// Load texture data (Cube)
		if (!GL_LoadTextureBMP(g_uiTextures[0], "../resources/Lab04/CrateDiffuse.bmp"))
			return false;
		if (!GL_LoadTextureBMP(g_uiTextures[1], "../resources/Lab04/CrateSpecular.bmp"))
			return false;
		if (!GL_LoadTextureBMP(g_uiTextures[2], "../resources/Lab04/CrateRoughness.bmp"))
			return false;
		// Load texture data (Sphere)
		if (!GL_LoadTextureBMP(g_uiTextures[3], "../resources/Lab04/WorldDiffuse.bmp"))
			return false;
		if (!GL_LoadTextureBMP(g_uiTextures[4], "../resources/Lab04/WorldSpecular.bmp"))
			return false;
		if (!GL_LoadTextureBMP(g_uiTextures[5], "../resources/Lab04/WorldRoughness.bmp"))
			return false;
		// Load texture data (Pyramid)
		if (!GL_LoadTextureBMP(g_uiTextures[6], "../resources/Lab04/PyramidDiffuse.bmp"))
			return false;
		if (!GL_LoadTextureBMP(g_uiTextures[7], "../resources/Lab04/PyramidSpecular.bmp"))
			return false;
		if (!GL_LoadTextureBMP(g_uiTextures[8], "../resources/Lab04/PyramidRoughness.bmp"))
			return false;
	}

	// Get the uniform location and bind to texture unit
	GLuint uiTextureU = glGetUniformLocation(g_uiProgram[0], "s2DiffuseTexture");
	glUniform1i(uiTextureU, 0);
	uiTextureU = glGetUniformLocation(g_uiProgram[0], "s2SpecularTexture");
	glUniform1i(uiTextureU, 1);
	uiTextureU = glGetUniformLocation(g_uiProgram[0], "s2RoughnessTexture");
	glUniform1i(uiTextureU, 2);


	// Specify program to use
	glUseProgram( g_uiProgram[0] );
	return true;
}

bool GL_LoadShaderResource(GLuint& uiVertexShader, int resourceNumber, int shaderType)
{
	bool success = false; // return var so there's no mem leaks
	HINSTANCE hInst = GetModuleHandle(NULL);	// create bogus handle
	HRSRC hRes = FindResource(hInst, MAKEINTRESOURCE(resourceNumber), RT_RCDATA);	// find resource data from resource file
	HGLOBAL hMem = LoadResource(hInst, hRes);	// load resource in to mem and pointer
	char * eShader = (char *)LockResource(hMem);

	success = GL_LoadShader(uiVertexShader, shaderType, eShader);	// Send shader data to next function (fail if necessary)

	FreeResource(hMem);	// Free the mem
	return success; // return success var
}
bool GL_LoadShader( GLuint & uiShader, GLenum ShaderType, const GLchar * p_cShader )
{
	// Build and link the shader program
	uiShader = glCreateShader( ShaderType );
	glShaderSource( uiShader, 1, &p_cShader, NULL );
	glCompileShader( uiShader );

	// Check for errors
	GLint iTestReturn;
	glGetShaderiv( uiShader, GL_COMPILE_STATUS, &iTestReturn );
	if( iTestReturn == GL_FALSE )
	{
		GLchar p_cInfoLog[1024];
		int32_t iErrorLength;
		glGetShaderInfoLog( uiShader, 1024, &iErrorLength, p_cInfoLog );
		SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION, "Failed to compile shader: %s\n", p_cInfoLog );

		// Delete the failed shader
		glDeleteShader( uiShader );
		return false;
	}
	return true;
}

bool GL_LoadShaders( GLuint & uiShader, GLuint uiVertexShader, GLuint uiFragmentShader )
{
	// Link the shaders
	uiShader = glCreateProgram( );
	glAttachShader( uiShader, uiVertexShader );
	glAttachShader( uiShader, uiFragmentShader );
	glLinkProgram( uiShader );

	//Check for error in link
	 GLint iTestReturn;
	 glGetProgramiv( uiShader, GL_LINK_STATUS, &iTestReturn );
	 if( iTestReturn == GL_FALSE )
	 {
		 GLchar p_cInfoLog[1024];
		 int32_t iErrorLength;
		 glGetShaderInfoLog( uiShader, 1024, &iErrorLength, p_cInfoLog );
		 SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION, "Failed to link shaders: %s\n", p_cInfoLog );
		 glDeleteProgram( uiShader );
		 return false;
	 }
	 return true;
}

// GL_Quit - modified for Lab02.30
void GL_Quit( )
{
	// Delete VBOs/IBOs and VAOs
	glDeleteBuffers( 3, &g_uiVBO[0] );
	glDeleteBuffers( 3, &g_uiIBO[0] );
	glDeleteVertexArrays( 3, &g_uiVAO[0] );

	glDeleteBuffers( 5, &g_uiTransformUBO[0] ); // Delete transform UBOs
	glDeleteBuffers( 1, &g_uiCameraUBO ); // Delete camera UBOs
	glDeleteTextures( 9, &g_uiTextures[0] ); // Delete textures
	glDeleteBuffers( 1, &g_uiPointLightUBO); // Delete point light UBO (all lights contained in this one)
}

void GL_Render( )
{
	// Clear the render output and depth buffer
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	// Specify Cube VAO (Lab02.28)
	glBindVertexArray( g_uiVAO[0] );
	// Bind the textures to texture units (Cube) (Replaces Material)
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, g_uiTextures[0]);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, g_uiTextures[1]);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, g_uiTextures[2]);

	// Bind the Transform UBO
	glBindBufferBase( GL_UNIFORM_BUFFER, 1, g_uiTransformUBO[0] );
	// Draw the Cube
	glDrawElements( GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0 );

	// Specify Sphere VAO (Lab02.29)
	glBindVertexArray( g_uiVAO[1] );
	// Bind the textures to texture units (Sphere / World) (Replaces Material)
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, g_uiTextures[3]);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, g_uiTextures[4]);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, g_uiTextures[5]);

	// Render each sphere
	for( int i = 1; i < 3; i++ )
	{
		// Bind the Transform UBO
		glBindBufferBase( GL_UNIFORM_BUFFER, 1, g_uiTransformUBO[i] );
		// Draw the Sphere
		glDrawElements( GL_TRIANGLES, g_iSphereElements, GL_UNSIGNED_INT, 0 );
	}

	// Specift Pyramid VAO
	glBindVertexArray( g_uiVAO[2] );
	// Bind the textures to texture units (Sphere / World) (Replaces Material)
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, g_uiTextures[6]);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, g_uiTextures[7]);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, g_uiTextures[8]);
	// Render each pyramid
	for( int i = 3; i < 5; i++ )
	{
		// Bind the Transform UBO
		glBindBufferBase( GL_UNIFORM_BUFFER, 1, g_uiTransformUBO[i] );
		// Draw the Sphere
		glDrawElements( GL_TRIANGLES, g_iPyramidElements, GL_UNSIGNED_INT, 0 );
	}
}

// Function to generate a cube
GLsizei GL_GenerateCube(GLuint uiVBO, GLuint uiIBO)
{
	// Vertexes
	CustomVertex VertexData[] =
	{
		// Create back face
		{ glm::vec4( 0.5f, 0.5f,-0.5f,1.0f), glm::vec4( 0.0f, 0.0f,-1.0f, 0.0f), glm::vec2(1.0f, 1.0f) },
		{ glm::vec4( 0.5f,-0.5f,-0.5f,1.0f), glm::vec4( 0.0f, 0.0f,-1.0f, 0.0f), glm::vec2(1.0f, 0.5f) },
		{ glm::vec4(-0.5f,-0.5f,-0.5f,1.0f), glm::vec4( 0.0f, 0.0f,-1.0f, 0.0f), glm::vec2(0.5f, 0.5f) },
		{ glm::vec4(-0.5f, 0.5f,-0.5f,1.0f), glm::vec4( 0.0f, 0.0f,-1.0f, 0.0f), glm::vec2(0.5f, 1.0f) },
		// Create left face
		{ glm::vec4(-0.5f, 0.5f,-0.5f,1.0f), glm::vec4(-1.0f, 0.0f, 0.0f, 0.0f), glm::vec2(0.5f, 1.0f) },
		{ glm::vec4(-0.5f,-0.5f,-0.5f,1.0f), glm::vec4(-1.0f, 0.0f, 0.0f, 0.0f), glm::vec2(0.5f, 0.5f) },
		{ glm::vec4(-0.5f,-0.5f, 0.5f,1.0f), glm::vec4(-1.0f, 0.0f, 0.0f, 0.0f), glm::vec2(0.0f, 0.5f) },
		{ glm::vec4(-0.5f, 0.5f, 0.5f,1.0f), glm::vec4(-1.0f, 0.0f, 0.0f, 0.0f), glm::vec2(0.0f, 1.0f) },
		// Create bottom face
		{ glm::vec4( 0.5f,-0.5f,-0.5f,1.0f), glm::vec4( 0.0f,-1.0f, 0.0f, 0.0f), glm::vec2(0.5f, 0.0f) },
		{ glm::vec4( 0.5f,-0.5f, 0.5f,1.0f), glm::vec4( 0.0f,-1.0f, 0.0f, 0.0f), glm::vec2(0.5f, 0.5f) },
		{ glm::vec4(-0.5f,-0.5f, 0.5f,1.0f), glm::vec4( 0.0f,-1.0f, 0.0f, 0.0f), glm::vec2(0.0f, 0.5f) },
		{ glm::vec4(-0.5f,-0.5f,-0.5f,1.0f), glm::vec4( 0.0f,-1.0f, 0.0f, 0.0f), glm::vec2(0.0f, 0.0f) },
		// Create front face
		{ glm::vec4(-0.5f, 0.5f, 0.5f,1.0f), glm::vec4( 0.0f, 0.0f, 1.0f, 0.0f), glm::vec2(1.0f, 1.0f) },
		{ glm::vec4(-0.5f,-0.5f, 0.5f,1.0f), glm::vec4( 0.0f, 0.0f, 1.0f, 0.0f), glm::vec2(1.0f, 0.5f) },
		{ glm::vec4( 0.5f,-0.5f, 0.5f,1.0f), glm::vec4( 0.0f, 0.0f, 1.0f, 0.0f), glm::vec2(0.5f, 0.5f) },
		{ glm::vec4( 0.5f, 0.5f, 0.5f,1.0f), glm::vec4( 0.0f, 0.0f, 1.0f, 0.0f), glm::vec2(0.5f, 1.0f) },
		// Create right face
		{ glm::vec4( 0.5f, 0.5f, 0.5f,1.0f), glm::vec4( 1.0f, 0.0f, 0.0f, 0.0f), glm::vec2(0.5f, 1.0f) },
		{ glm::vec4( 0.5f,-0.5f, 0.5f,1.0f), glm::vec4( 1.0f, 0.0f, 0.0f, 0.0f), glm::vec2(0.5f, 0.5f) },
		{ glm::vec4( 0.5f,-0.5f,-0.5f,1.0f), glm::vec4( 1.0f, 0.0f, 0.0f, 0.0f), glm::vec2(0.0f, 0.5f) },
		{ glm::vec4( 0.5f, 0.5f,-0.5f,1.0f), glm::vec4( 1.0f, 0.0f, 0.0f, 0.0f), glm::vec2(0.0f, 1.0f) },
		// Create top face
		{ glm::vec4( 0.5f, 0.5f, 0.5f,1.0f), glm::vec4( 0.0f, 1.0f, 0.0f, 0.0f), glm::vec2(1.0f, 0.5f) },
		{ glm::vec4( 0.5f, 0.5f,-0.5f,1.0f), glm::vec4( 0.0f, 1.0f, 0.0f, 0.0f), glm::vec2(1.0f, 0.0f) },
		{ glm::vec4(-0.5f, 0.5f,-0.5f,1.0f), glm::vec4( 0.0f, 1.0f, 0.0f, 0.0f), glm::vec2(0.5f, 0.0f) },
		{ glm::vec4(-0.5f, 0.5f, 0.5f,1.0f), glm::vec4( 0.0f, 1.0f, 0.0f, 0.0f), glm::vec2(0.5f, 0.5f) },
	}; 

	// Create the cube, front facing triangles must be clockwise
	GLuint uiIndexData[] =
	{
		 0, 1, 3, 3, 1, 2, // Create back face
		 4, 5, 7, 7, 5, 6, // Create left face
		 8, 9, 11, 11, 9, 10, // Create bottom face
		 12, 13, 15, 15, 13, 14, // Create front face
		 16, 17, 19, 19, 17, 18, // Create right face
		 20, 21, 23, 23, 21, 22 // Create top face
	};

	// Fill Vertex Buffer Object
	glBindBuffer( GL_ARRAY_BUFFER, uiVBO );
	glBufferData( GL_ARRAY_BUFFER, sizeof( VertexData ), VertexData, GL_STATIC_DRAW );
	// Fill Index Buffer Object
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, uiIBO );
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( uiIndexData ), uiIndexData, GL_STATIC_DRAW );
	// Specify location of data within buffer
	// - Location of vertices
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(CustomVertex), (const GLvoid *)0);
	glEnableVertexAttribArray(0);
	// - Location of normals
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(CustomVertex), (const GLvoid *)offsetof(CustomVertex, v4Normal));
	glEnableVertexAttribArray(1);
	// - UV coordinates
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(CustomVertex), (const GLvoid *)offsetof(CustomVertex, v2UV));
	glEnableVertexAttribArray(2);

	return ( sizeof( uiIndexData ) / sizeof( GLuint ) );
}

// Function to generate a pyramid
GLsizei GL_GeneratePyramid(GLuint uiVBO, GLuint uiIBO)
{
	// Vertexes
	CustomVertex VertexData[] =
	{ //0.707107
		// Front Face (+z,+y normals)
		{ glm::vec4( -0.5f, -0.5f,  0.5f, 1.0f ), glm::vec4( 0.0f, 0.707107f, 0.707107f,0.0f), glm::vec2(0.0f, 0.0f) }, //front left
		{ glm::vec4(  0.5f, -0.5f,  0.5f, 1.0f ), glm::vec4( 0.0f, 0.707107f, 0.707107f,0.0f), glm::vec2(1.0f, 0.0f) }, //front right
		{ glm::vec4(  0.0f,  0.5f,  0.0f, 1.0f ), glm::vec4( 0.0f, 0.707107f, 0.707107f,0.0f), glm::vec2(0.5f, 1.0f) }, //top
		// Right Face (+x, +y)
		{ glm::vec4(  0.5f, -0.5f,  0.5f, 1.0f ), glm::vec4( 0.707107f, 0.707107f, 0.0f,0.0f), glm::vec2(0.0f, 0.0f) }, //front right
		{ glm::vec4(  0.5f, -0.5f, -0.5f, 1.0f ), glm::vec4( 0.707107f, 0.707107f, 0.0f,0.0f), glm::vec2(1.0f, 0.0f) }, //back right
		{ glm::vec4(  0.0f,  0.5f,  0.0f, 1.0f ), glm::vec4( 0.707107f, 0.707107f, 0.0f,0.0f), glm::vec2(0.5f, 1.0f) }, //top
		// Back Face (-z, +y)
		{ glm::vec4(  0.5f, -0.5f, -0.5f, 1.0f ), glm::vec4( 0.0f, 0.707107f, -0.707107f,0.0f), glm::vec2(0.0f, 0.0f) }, //back right
		{ glm::vec4( -0.5f, -0.5f, -0.5f, 1.0f ), glm::vec4( 0.0f, 0.707107f, -0.707107f,0.0f), glm::vec2(1.0f, 0.0f) }, //back left
		{ glm::vec4(  0.0f,  0.5f,  0.0f, 1.0f ), glm::vec4( 0.0f, 0.707107f, -0.707107f,0.0f), glm::vec2(0.5f, 1.0f) }, //top
		// Left Face (-x, +y)
		{ glm::vec4( -0.5f, -0.5f, -0.5f, 1.0f ), glm::vec4( -0.707107f, 0.707107f, 0.0f,0.0f), glm::vec2(0.0f, 0.0f) }, //back left
		{ glm::vec4( -0.5f, -0.5f,  0.5f, 1.0f ), glm::vec4( -0.707107f, 0.707107f, 0.0f,0.0f), glm::vec2(1.0f, 0.0f) }, //front left
		{ glm::vec4(  0.0f,  0.5f,  0.0f, 1.0f ), glm::vec4( -0.707107f, 0.707107f, 0.0f,0.0f), glm::vec2(0.5f, 1.0f) }, //top
		// Bottom Face
		// Left Rear (similar to cube back)
		{ glm::vec4(  0.5f, -0.5f,  0.5f, 1.0f ), glm::vec4( 0.0f, -1.0f, 0.0f,0.0f), glm::vec2(1.0f, 1.0f) }, //front right 12
		{ glm::vec4(  0.5f, -0.5f, -0.5f, 1.0f ), glm::vec4( 0.0f, -1.0f, 0.0f,0.0f), glm::vec2(1.0f, 0.0f) }, //back right  13
		{ glm::vec4( -0.5f, -0.5f, -0.5f, 1.0f ), glm::vec4( 0.0f, -1.0f, 0.0f,0.0f), glm::vec2(0.0f, 0.0f) }, //back left   14
		{ glm::vec4( -0.5f, -0.5f,  0.5f, 1.0f ), glm::vec4( 0.0f, -1.0f, 0.0f,0.0f), glm::vec2(0.0f, 1.0f) }, //front left  15
	};

	// Create the cube, front facing triangles must be clockwise
	GLuint uiIndexData[] = {
		//Create front face
		0, 1, 2,
		//Create right face
		3, 4, 5,
		//Create back face
		6, 7, 8,
		//Create left face
		9, 10, 11,
		//Create bottom face
		12, 15, 13, 15, 14, 13
	};

	// Fill Vertex Buffer Object
	glBindBuffer( GL_ARRAY_BUFFER, uiVBO );
	glBufferData( GL_ARRAY_BUFFER, sizeof( VertexData ), VertexData, GL_STATIC_DRAW );
	// Fill Index Buffer Object
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, uiIBO );
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( uiIndexData ), uiIndexData, GL_STATIC_DRAW );
	// Specify location of data within buffer
	// - Location of vertices
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(CustomVertex), (const GLvoid *)0);
	glEnableVertexAttribArray(0);
	// - Location of normals
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(CustomVertex), (const GLvoid *)offsetof(CustomVertex, v4Normal));
	glEnableVertexAttribArray(1);
	// - UV coordinates
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(CustomVertex), (const GLvoid *)offsetof(CustomVertex, v2UV));
	glEnableVertexAttribArray(2);

	return ( sizeof( uiIndexData ) / sizeof( GLuint ) );
}

// Function to generate a sphere
GLsizei GL_GenerateSphere( unsigned int uiTessU, unsigned int uiTessV, GLuint uiVBO, GLuint uiIBO)
{
	// Init params
	float fDPhi = (float)M_PI / (float)uiTessV;
	float fDTheta = (float)( M_PI + M_PI ) / (float)uiTessU;
	// Determine required parameters
	uiTessU = uiTessU + 1; // Add extra col of vertices for texture mapping
	unsigned int uiNumVertices = ( uiTessU * ( uiTessV - 1 ) ) + 2;
	unsigned int uiNumIndices = ( uiTessU * 6 ) + ( uiTessU * ( uiTessV-2 ) * 6 );
	// Create the new primitive
	CustomVertex * p_VBuffer = (CustomVertex *)malloc( uiNumVertices * sizeof( CustomVertex ) );
	GLuint * p_IBuffer = (GLuint *)malloc( uiNumIndices * sizeof( GLuint ) );

	// Set the top and bottom vertex and reuse
	CustomVertex * p_vBuffer = p_VBuffer;
	p_vBuffer->v4Position =	glm::vec4( 0.0f, 1.0f, 0.0f, 1.0f );
	p_vBuffer->v4Normal = glm::vec4( 0.0f, 1.0f, 0.0f, 0.0f );
	p_vBuffer->v2UV = glm::vec2(0.5f, 1.0f);
	p_vBuffer[uiNumVertices - 1].v4Position = glm::vec4( 0.0f, -1.0f, 0.0f, 1.0f );
	p_vBuffer[uiNumVertices - 1].v4Normal = glm::vec4(0.0f, -1.0f, 0.0f, 0.0f);
	p_vBuffer[uiNumVertices - 1].v2UV = glm::vec2(0.5f, 0.0f);
	p_vBuffer++;

	float fPhi = fDPhi;
	for( unsigned int uiPhi = 0; uiPhi < uiTessV - 1; uiPhi++ )
	{
		// Calculate initial value
		float fRSinPhi = sinf( fPhi );
		float fRCosPhi = cosf( fPhi );
		float fY = fRCosPhi;
		float fTheta = 0.0f;
		for( unsigned int uiTheta = 0; uiTheta < uiTessU; uiTheta++ )
		{
			// Calculate positions
			float fCosTheta = cosf( fTheta );
			float fSinTheta = sinf( fTheta );
			// Determine position
			float fX = fRSinPhi * fCosTheta;
			float fZ = fRSinPhi * fSinTheta;
			// Create vertex
			p_vBuffer->v4Position = glm::vec4( fX, fY, fZ, 1.0f );
			p_vBuffer->v4Normal = glm::vec4( fX, fY, fZ, 0.0f );
			p_vBuffer->v2UV = glm::vec2(1.0f - (fTheta / (float)(M_PI + M_PI)), 1.0f - (fPhi / (float)M_PI));
			p_vBuffer++;
			fTheta += fDTheta;
		}
		fPhi += fDPhi;
	}

	// Create top
	GLuint * p_iBuffer = p_IBuffer;
	for( GLuint j = 1; j <= uiTessU; j++ )
	{
		// Top triangles all share same vertex point at pos 0
		*p_iBuffer++ = 0;
		// Loop back to start if required
		*p_iBuffer++ = ( ( j + 1 ) > uiTessU )? 1 : j + 1;
		*p_iBuffer++ = j;
	}

	// Create inner triangles
	for( GLuint i = 0; i < uiTessV - 2; i++ )
	{
		for( GLuint j = 1; j <= uiTessU; j++ )
		{
			//create indexes for each quad face (pair of triangles)
			*p_iBuffer++ = j + ( i * uiTessU );
			// Loop back to start if required
			GLuint Index = ( ( j + 1 ) > uiTessU )? 1 : j + 1;
			*p_iBuffer++ = Index + ( i * uiTessU );
			*p_iBuffer++ = j + ( ( i + 1 ) * uiTessU );
			*p_iBuffer = *( p_iBuffer - 2 );
			p_iBuffer++;
			// Loop back to start if required
			*p_iBuffer++ = Index + ( ( i + 1 ) * uiTessU );
			*p_iBuffer = *( p_iBuffer - 3 );
			p_iBuffer++;
		}
	}

	// Create bottom
	for( GLuint j = 1; j <= uiTessU; j++ )
	{
		// Bottom triangles all share same vertex uiNumVertices - 1
		*p_iBuffer++ = j + ( ( uiTessV - 2 ) * uiTessU );
		// Loop back to start if required
		GLuint Index = ( ( j + 1 ) > uiTessU ) ? 1 : j + 1;
		*p_iBuffer++ = Index + ( ( uiTessV - 2 ) * uiTessU );
		*p_iBuffer++ = uiNumVertices - 1;
	}

	// Fill Vertex Buffer Object
	glBindBuffer( GL_ARRAY_BUFFER, uiVBO );
	glBufferData( GL_ARRAY_BUFFER, uiNumVertices * sizeof( CustomVertex ), p_VBuffer, GL_STATIC_DRAW );
	// Fill Index Buffer Object
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, uiIBO );
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, uiNumIndices * sizeof( GLuint ), p_IBuffer, GL_STATIC_DRAW );
	// Cleanup allocated data
	free( p_VBuffer ); free( p_IBuffer );
	// Specify location of data within buffer
	// - Location of vertices
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(CustomVertex), (const GLvoid *)0);
	glEnableVertexAttribArray(0);
	// - Location of normals
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(CustomVertex), (const GLvoid *)offsetof(CustomVertex, v4Normal));
	glEnableVertexAttribArray(1);
	// - UV Coords
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(CustomVertex), (const GLvoid *)offsetof(CustomVertex, v2UV));
	glEnableVertexAttribArray(2);

	return uiNumIndices;
}

void GL_Update(float fElapsedTime)
{
	// Calculate new rotation value
	g_objectRotation = fElapsedTime * degreesPerSecond;

	for (int i = 1; i < 5; i++)
	{
		// For trippy effect (*IGNORE MEEEE*)
		//g_m4Transform[i] = glm::rotate(g_m4Transform[i], g_objectRotation, glm::vec3(0, 0, 1));
		
		// Orbit Object * Object Position * Rotate Object << this way
		g_m4Transform[i] = glm::rotate(glm::mat4(1.0f), g_objectRotation, glm::vec3(0, 0, 1)) * g_m4Transform[i] * glm::rotate(glm::mat4(1.0f), g_objectRotation * i * 1.0f, glm::vec3(0, 1, 1));

		// Orbit Object * Object Position << this way
		//g_m4Transform[i] = glm::rotate(glm::mat4(1.0f), g_objectRotation, glm::vec3(0, 0, 1)) * g_m4Transform[i];
	}

	// Orbit objects around cube (Apply Transforms)
	for (int i = 0; i < 5; i++)
	{
		glBindBuffer(GL_UNIFORM_BUFFER, g_uiTransformUBO[i]);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), &g_m4Transform[i], GL_STREAM_DRAW);
	}

	// Update the cameras position
	g_CameraData.m_v3Position += g_CameraData.m_fMoveZ * fElapsedTime * g_CameraData.m_v3Direction;
	g_CameraData.m_v3Position += g_CameraData.m_fMoveX * fElapsedTime * g_CameraData.m_v3Right;

	// Determine rotation matrix for camera angles
	glm::mat4 m4Rotation = glm::rotate(glm::mat4(1.0f),g_CameraData.m_fAngleX, glm::vec3(0.0f, 1.0f, 0.0f));
	m4Rotation = glm::rotate(m4Rotation, g_CameraData.m_fAngleY,glm::vec3(1.0f, 0.0f, 0.0f));

	// Determine new view and right vectors
	g_CameraData.m_v3Direction = glm::mat3(m4Rotation) * glm::vec3(0.0f, 0.0f, 1.0f);
	g_CameraData.m_v3Right = glm::mat3(m4Rotation) * glm::vec3(-1.0f, 0.0f, 0.0f);

	// Set Mouse capture and hide cursor
	SDL_ShowCursor(0);
	SDL_SetRelativeMouseMode(SDL_TRUE);

	// Re-Create camera view matrix
	glm::mat4 m4View = glm::lookAt(
		g_CameraData.m_v3Position,
		g_CameraData.m_v3Position + g_CameraData.m_v3Direction,
		glm::cross(g_CameraData.m_v3Right, g_CameraData.m_v3Direction)
		);
	// Re-Create camera projection matrix
	glm::mat4 m4Projection = glm::perspective(
		g_CameraData.m_fFOV,
		g_CameraData.m_fAspect,
		g_CameraData.m_fNear, g_CameraData.m_fFar
		);
	// Create ViewProjection matrix
	CameraData Camera = {
		m4Projection * m4View,						// View Projection
		glm::vec4(g_CameraData.m_v3Position, 1.0)	// Position
	};
	// Update the camera buffer
	glBindBuffer(GL_UNIFORM_BUFFER, g_uiCameraUBO);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(CameraData), &Camera, GL_STREAM_DRAW);
}

bool GL_LoadTextureBMP(GLuint uiTexture, const char * p_cTextureFile)
{
	// Load texture data - returns pointer with data / res / depth / ??
	SDL_Surface * p_Surface = SDL_LoadBMP(p_cTextureFile);
	if (p_Surface == NULL)
	{
		SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "Failed to open texture file : %s\n", SDL_GetError( ) );
		return false;
	}

	// Determine image format (or error)
	GLenum Format;
	GLint iInternalFormat;
	if (p_Surface->format->BytesPerPixel == 4)
	{
		iInternalFormat = GL_RGBA;
		if (p_Surface->format->Rmask == 0x000000ff)
			Format = GL_RGBA;
		else
			Format = GL_BGRA;
	}
	else if (p_Surface->format->BytesPerPixel == 3)
	{
		iInternalFormat = GL_RGBA;
		if (p_Surface->format->Rmask == 0x000000ff)
			Format = GL_RGB;
		else
			Format = GL_BGR;
	}
	else if (p_Surface->format->BytesPerPixel == 1)
	{
		iInternalFormat = GL_RED;
		Format = GL_RED;
	}
	else
	{
		SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "Unknown texture format : %d\n", p_Surface->format->BytesPerPixel );
		return false;
	}

	// Correctly flip image data
	const int iRowSize = p_Surface->w * p_Surface->format->BytesPerPixel;
	const int iImageSize = iRowSize * p_Surface->h;
	GLubyte * p_TextureData = (GLubyte*)malloc(iImageSize);
	for (int i = 0; i < p_Surface->h * iRowSize; i += iRowSize)
	{
		memcpy(&p_TextureData[i], &((GLubyte*)p_Surface->pixels)[iImageSize - i], iRowSize);
	}

	// Bind texture
	glBindTexture( GL_TEXTURE_2D, uiTexture );

	// Copy data into texture
	glTexImage2D(GL_TEXTURE_2D, 0, iInternalFormat, p_Surface->w, p_Surface->h, 0, Format, GL_UNSIGNED_BYTE, p_TextureData);
	// Unload the SDL surface
	SDL_FreeSurface(p_Surface);
	free(p_TextureData);

	// Generate mipmaps
	glGenerateMipmap(GL_TEXTURE_2D);

	// Initialise the texture filtering values
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	return true;
}

bool GL_LoadTextureKTX(GLuint uiTexture, const char * p_cTextureFile)
{
	// Load texture data
	GLenum GLTarget, GLError;
	GLboolean bIsMipmapped;
	KTX_error_code ktxerror = ktxLoadTextureN(p_cTextureFile, &uiTexture, &GLTarget, NULL, &bIsMipmapped, &GLError, 0, NULL);
	if (ktxerror != KTX_SUCCESS)
	{
		SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "Failed to read texture file : %s\n", ktxErrorString( ktxerror ) );
			return false;
	}

	// Generate mipmaps 
	if( !bIsMipmapped ) 
		glGenerateMipmap( GL_TEXTURE_2D );

	// Initialise the texture filtering values
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	return true;
}