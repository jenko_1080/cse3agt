#version 440
// Defines //
/////////////
#define M_RCPPI 0.31830988618379067153776752674503
#define M_PI 3.1415926535897932384626433832795

// Data Structures //
/////////////////////
//struct PointLight { 
//	vec3 v3LightPosition; 
//	vec4 v4LightIntensity; 
//	float fFalloff; 
//};
//uniform PointLightData {
//	PointLight PointLights;
//};
//uniform MaterialData {
//	vec4 v4DiffuseColour;
//	vec4 v4SpecularColour;
//	float fRoughness;
//};
uniform TransformData {
	mat4 m4Transform;
};
uniform CameraData {
	mat4 m4ViewProjection;
	vec3 v3CameraPosition;
};

// Inputs
layout( location = 0 ) in vec4 v4VertexPos;
layout( location = 1 ) in vec4 v4VertexNormal;
layout( location = 2 ) in vec2 v2VertexUV;
// Outputs
layout( location = 0 ) smooth out vec4 v4Position;
layout( location = 1 ) smooth out vec4 v4Normal;
layout( location = 2 ) smooth out vec2 v2UV;

// Function Prototypes //
/////////////////////////
//vec3 blinnPhong( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in vec3 v3LightIrradiance );
//vec3 lightFalloff( in vec3 v3LightIntensity, in float fFalloff, in vec3 v3LightPosition, in vec3 v3Position );

void main() {
	// Transform vertex
	v4Position = m4Transform * v4VertexPos;
	gl_Position = m4ViewProjection * v4Position;
	// Transform Normal
	v4Normal = m4Transform * v4VertexNormal;
	// Pass through UV coords
	v2UV = v2VertexUV;
}

// Functions //
///////////////
//vec3 blinnPhong( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in vec3 v3LightIrradiance ) {
//	// Calculate diffuse component
//	vec3 v3Diffuse = v4DiffuseColour.rgb;
//	// Calculate half vector
//	vec3 v3HalfVector = normalize( v3ViewDirection + v3LightDirection );
//	// Calculate specular component
//	vec3 v3Specular = pow( max( dot( v3Normal, v3HalfVector ), 0.0 ), fRoughness ) * v4SpecularColour.rgb;
//	// Normalize diffuse and specular component
//	v3Diffuse *= M_RCPPI;
//	v3Specular *= ( fRoughness + 8 ) / ( 8 * M_PI );
//	//Combine diffuse and specular
//	vec3 v3RetColour = v3Diffuse + v3Specular;
//	// Multiply by view angle
//	v3RetColour *= max( dot( v3Normal, v3LightDirection ), 0.0 );
//	// Combine with incoming light value 
//	v3RetColour *= v3LightIrradiance;
//	return v3RetColour;
//}
//
//vec3 lightFalloff( in vec3 v3LightIntensity, in float fFalloff, in vec3 v3LightPosition, in vec3 v3Position )
//{
//	// Calculate distance from light
//	float fDist = distance( v3LightPosition, v3Position );
//	// Return fallof
//	return v3LightIntensity / ( fFalloff * fDist * fDist );
//}
