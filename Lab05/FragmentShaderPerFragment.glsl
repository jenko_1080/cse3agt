#version 440
// Defines //
/////////////
#define M_RCPPI 0.31830988618379067153776752674503
#define M_PI 3.1415926535897932384626433832795

// Data Structures //
/////////////////////
struct PointLight { 
	vec3 v3LightPosition; 
	vec4 v4LightIntensity; 
	float fFalloff; 
};
uniform PointLightData {
	PointLight PointLights;
};
uniform MaterialData {
	vec4 v4DiffuseColour;
	vec4 v4SpecularColour;
	float fRoughness;
};
uniform CameraData {
	mat4 m4ViewProjection;
	vec3 v3CameraPosition;
};

// Inputs
layout( location = 0 ) in vec4 v4Position;
layout( location = 1 ) in vec4 v4Normal;
// Outputs
out vec4 v4ColourOut;

// Function Prototypes //
/////////////////////////
vec3 blinnPhong( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in vec3 v3LightIrradiance );
vec3 lightFalloff( in vec3 v3LightIntensity, in float fFalloff, in vec3 v3LightPosition, in vec3 v3Position );

void main() {
	// Normalize the inputs
	vec3 v3TNormal = normalize( v4Normal.xyz );
	vec3 v3TViewDir = normalize( v3CameraPosition - v4Position.xyz );
	vec3 v3TLightDir = normalize( PointLights.v3LightPosition -	v4Position.xyz );

	// Calculate light falloff
	vec3 v3LightIrradiance = lightFalloff( PointLights.v4LightIntensity.rgb, PointLights.fFalloff, PointLights.v3LightPosition, v4Position.xyz );

	// Perform shading
	vec3 v3ColourOut = blinnPhong( v3TNormal, v3TLightDir, v3TViewDir, v3LightIrradiance );
	// Add Diffuse Lighting
	v3ColourOut += v4DiffuseColour.xyz * vec3( 0.05, 0.05, 0.05 );
	v4ColourOut = vec4( v3ColourOut, 1.0 );
	//v4ColourOut = vec4( 1.0, 1.0, 1.0, 1.0 ); //<-- AIDEN: this works
}

// Functions //
///////////////
vec3 blinnPhong( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in vec3 v3LightIrradiance ) {
	// Calculate diffuse component
	vec3 v3Diffuse = v4DiffuseColour.rgb;
	// Calculate half vector
	vec3 v3HalfVector = normalize( v3ViewDirection + v3LightDirection );
	// Convert roughness to Phong shininess
	float fRoughnessPhong = ( 2.0 / ( fRoughness * fRoughness ) ) - 2;
	// Calculate specular component
	vec3 v3Specular = pow( max( dot( v3Normal, v3HalfVector ), 0.0 ), fRoughnessPhong ) * v4SpecularColour.rgb;
	// Normalize diffuse and specular component
	v3Diffuse *= M_RCPPI;
	v3Specular *= ( fRoughnessPhong + 8 ) / ( 8 * M_PI );
	//Combine diffuse and specular
	vec3 v3RetColour = v3Diffuse + v3Specular;
	// Multiply by view angle
	v3RetColour *= max( dot( v3Normal, v3LightDirection ), 0.0 );
	// Combine with incoming light value 
	v3RetColour *= v3LightIrradiance;
	return v3RetColour;
}

vec3 lightFalloff( in vec3 v3LightIntensity, in float fFalloff, in vec3 v3LightPosition, in vec3 v3Position )
{
	// Calculate distance from light
	float fDist = distance( v3LightPosition, v3Position );
	// Return fallof
	return v3LightIntensity / ( fFalloff * fDist * fDist );
}