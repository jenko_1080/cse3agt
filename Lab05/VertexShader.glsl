#version 440
// Defines //
/////////////
#define M_RCPPI 0.31830988618379067153776752674503
#define M_PI 3.1415926535897932384626433832795

// Data Structures //
/////////////////////
struct PointLight { 
	vec3 v3LightPosition; 
	vec4 v4LightIntensity; 
	vec3 v3Falloff;  
};
// Allow up to MAX_LIGHTS light sources
#define MAX_LIGHTS 8
uniform PointLightData {
	PointLight PointLights[MAX_LIGHTS];
};
uniform int iNumPointLights;
uniform MaterialData {
	vec4 v4DiffuseColour;
	vec4 v4SpecularColour;
	float fRoughness;
};
uniform TransformData {
	mat4 m4Transform;
};
uniform CameraData {
	mat4 m4ViewProjection;
	vec3 v3CameraPosition;
};

// Inputs
layout( location = 0 ) in vec4 v4VertexPos;
layout( location = 1 ) in vec4 v4VertexNormal;
// Outputs
layout( location = 0 ) smooth out vec4 v4ColourOut;

// Function Prototypes //
/////////////////////////
vec3 blinnPhong( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in vec3 v3LightIrradiance );
vec3 lightFalloff( in vec3 v3LightIntensity, in vec3 v3Falloff, in vec3 v3LightPosition, in vec3 v3Position );

void main() {
	// Transform vertex
	vec4 v4Position = m4Transform * v4VertexPos;
	gl_Position = m4ViewProjection * v4Position;
	// Transform Normal
	vec4 v4Normal = m4Transform * v4VertexNormal;
	// Normalize the inputs
	vec3 v3TNormal = normalize( v4Normal.xyz );
	vec3 v3TViewDir = normalize( v3CameraPosition - v4Position.xyz );
	vec3 v3ColourOut = vec3(0.0, 0.0, 0.0);

	for(int i = 0; i < iNumPointLights; ++i)
	{
		vec3 v3TLightDir = normalize( PointLights[i].v3LightPosition - v4Position.xyz );
		// Calculate light falloff
		vec3 v3LightIrradiance = lightFalloff( PointLights[i].v4LightIntensity.rgb, PointLights[i].v3Falloff, PointLights[i].v3LightPosition, v4Position.xyz );
		// Perform shading and ADD to current output
		v3ColourOut += blinnPhong( v3TNormal, v3TLightDir, v3TViewDir, v3LightIrradiance );
	}

	// Add Diffuse Lighting
	//v3ColourOut += v4DiffuseColour.xyz * vec3( 0.05, 0.05, 0.05 );
	v4ColourOut = vec4( v3ColourOut, 1.0 );
}

// Functions //
///////////////
vec3 blinnPhong( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in vec3 v3LightIrradiance ) {
	// Calculate diffuse component
	vec3 v3Diffuse = v4DiffuseColour.rgb;
	// Calculate half vector
	vec3 v3HalfVector = normalize( v3ViewDirection + v3LightDirection );
	// Calculate specular component
	vec3 v3Specular = pow( max( dot( v3Normal, v3HalfVector ), 0.0 ), fRoughness ) * v4SpecularColour.rgb;
	// Normalize diffuse and specular component
	v3Diffuse *= M_RCPPI;
	v3Specular *= ( fRoughness + 8 ) / ( 8 * M_PI );
	//Combine diffuse and specular
	vec3 v3RetColour = v3Diffuse + v3Specular;
	// Multiply by view angle
	v3RetColour *= max( dot( v3Normal, v3LightDirection ), 0.0 );
	// Combine with incoming light value 
	v3RetColour *= v3LightIrradiance;
	return v3RetColour;
}

vec3 lightFalloff( in vec3 v3LightIntensity, in vec3 v3Falloff, in vec3 v3LightPosition, in vec3 v3Position )
{
	// Calculate distance from light
	float fDist = distance( v3LightPosition, v3Position );
	// Return fallof
	//return v3LightIntensity / ( fFalloff * fDist * fDist );
	return v3LightIntensity / (( v3Falloff[0]) + ( v3Falloff[1] * fDist ) + ( v3Falloff[2] * fDist * fDist ));
}
