#define _CRT_SECURE_NO_WARNINGS

// Using SDL, SDL OpenGL, GLEW
#define GLEW_STATIC
#include <GL/glew.h>
#include <SDL.h>
#include <SDL_opengl.h>
// GLM and math headers (lab02)
#include <math.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
// Using KTX import 
#define KTX_OPENGL 1 
#include <ktx.h>
// Using Assimp
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

// Declare OpenGL variables
GLuint g_uiProgram[3];
GLuint g_uiSubRoutines[2]; // Storage for subroutines

// Vars for rotation
float g_objectRotation; // In degrees (0 to 360)
float degreesPerSecond = 1.00f;

// Struct for custom vertex
struct CustomVertex
{
	glm::vec4 v4Position;
	glm::vec4 v4Normal;
	glm::vec2 v2UV; // for texture information
};
// Struct to pass camera data
struct CameraData
{
	glm::mat4 m_m4ViewProjection;
	glm::vec4 m_v4Position;
};

// Point Light (radiates all directions)
// Position, Colour, Falloff
struct PointLightData
{
	glm::vec4 m_v4Position;
	glm::vec4 m_v4Colour;
	glm::vec3 m_v3Falloff;
	float pad;				// Changed for memory alignment? (thanks Tom S) (vec3 would also work, glm fixes it)
};
// Struct for Camera definition
struct LocalCameraData
{
	float m_fAngleX;
	float m_fAngleY;
	float m_fMoveZ;	// Lab2.33
	float m_fMoveX; // Lab2.33
	glm::vec3 m_v3Position;
	glm::vec3 m_v3Direction;
	glm::vec3 m_v3Right;
	float m_fFOV;
	float m_fAspect;
	float m_fNear;
	float m_fFar;
};

// Struct for Assimp returned mesh data
struct MeshData
{
	GLuint m_uiVAO;
	GLuint m_uiVBO;
	GLuint m_uiIBO;
	unsigned m_uiNumIndices;
};
// Struct for Assimp returned material data
// LAB05 Note: (Most files loaded by Assimp are unlikely to expose data exactly like this however for our purposes we will assume that it will).
struct MaterialData
{
	GLuint m_uiDiffuse;
	GLuint m_uiSpecular;
	GLuint m_uiRough;
};
// Struct for objects
// Copy of all data to simplify 'things' and increase performance
struct ObjectData
{
	GLuint m_uiVAO;
	unsigned m_uiNumIndices;
	GLuint m_uiDiffuse;
	GLuint m_uiSpecular;
	GLuint m_uiRough;
	glm::mat4 m_4Transform;
	GLuint m_uiTransformUBO;
};
// Struct for Scene data, complete list of all items and objects
// Using pointers for C, C++ can use vector (or similar)
struct SceneData
{
	MeshData * mp_Meshes;
	unsigned m_uiNumMeshes;
	MaterialData * mp_Materials;
	unsigned m_uiNumMaterials;
	ObjectData * mp_Objects;
	unsigned m_uiNumObjects;
	PointLightData * mp_Lights;
	unsigned m_uiNumLights;
	GLuint m_uiPointLightUBO;
	LocalCameraData m_LocalCamera;
	GLuint m_uiCameraUBO;
};
SceneData g_SceneData = {}; // Init to 0

bool g_bLoadKTX = true; // Var to tell the program to load a KTX instead of BMP
bool g_bRelativeMouse = true; // Var to lock or unlock the mouse (good for debug)
// Texture (was Material) Data
GLuint g_uiTextures[9];

//LocalCameraData g_CameraData;
//GLuint g_uiCameraUBO;
//GLuint g_uiPointLightUBO;

bool GL_Init(int iWindowWidth, int iWindowHeight);
bool GL_LoadShaderResource(GLuint& uiVertexShader, int resourceNumber, int shaderType);
bool GL_LoadShader(GLuint & uiShader, GLenum ShaderType, const GLchar * p_cShader);
bool GL_LoadShaders(GLuint & uiShader, GLuint uiVertexShader, GLuint uiFragmentShader);
bool GL_LoadTextureBMP(GLuint uiTexture, const char * p_cTextureFile);
bool GL_LoadTextureKTX(GLuint uiTexture, const char * p_cTextureFile);
bool GL_LoadScene(const char * p_cSceneFile, SceneData & SceneInfo, int iWindowWidth, int iWindowHeight);
void GL_LoadSceneNode(aiNode * p_Node, const aiScene * p_Scene, SceneData & SceneInfo, const glm::mat4 & m4Transform, bool bAddObjects);
void GL_UnloadScene(SceneData & SceneInfo);
void GL_Update(float fElapsedTime);
void GL_Quit();
void GL_Render();

#ifdef _WIN32
int WINAPI WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance,
	_In_ LPSTR lpCmdLine, _In_ int)
#else
int main(int argc, char **argv)
#endif
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION,
			"Failed to initialize SDL: %s\n", SDL_GetError());
		return 1;
	}

	// Use OpenGL 4.4 core profile
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	// Turn on double buffering 24bit Z buffer
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

	// Create a SDL window
	const int iWindowWidth = 1280;
	const int iWindowHeight = 1024;
	const bool bWindowFullscreen = false;

	// Get Desktop Resolution
	SDL_DisplayMode CurrentDisplay;
	SDL_GetCurrentDisplayMode(0, &CurrentDisplay);

	SDL_Window * Window = SDL_CreateWindow("AGT Tutorial",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, bWindowFullscreen ? CurrentDisplay.w : iWindowWidth, bWindowFullscreen ? CurrentDisplay.h : iWindowHeight,
		SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | (bWindowFullscreen * SDL_WINDOW_FULLSCREEN));
	if (Window == NULL)
	{
		SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "Failed to create OpenGL window: %s\n", SDL_GetError());
		SDL_Quit();
		return 1;
	}

	// Create OpenGL Context
	SDL_GLContext Context = SDL_GL_CreateContext(Window);
	if (Context == NULL)
	{
		SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "Failed to create OpenGL context: %s\n", SDL_GetError());
		SDL_DestroyWindow(Window);
		SDL_Quit();
		return 1;
	}

	// VSync (-1 = enable late swaps)
	SDL_GL_SetSwapInterval(-1);

	// Initialize OpenGL
	if (GL_Init(iWindowWidth, iWindowHeight)) // User made functions
	{
		//Initialise elapsed time
		Uint32 uiOldTime, uiCurrentTime;
		uiCurrentTime = SDL_GetTicks();

		// Start the program message pump (giggity)
		SDL_Event Event;
		bool bQuit = false;
		while (!bQuit)
		{
			// Update elapsed frame time (for justice and glory)
			uiOldTime = uiCurrentTime;
			uiCurrentTime = SDL_GetTicks();
			float fElapsedTime = (float)(uiCurrentTime - uiOldTime) / 1000.0f;

			// Poll SDL for buffered events
			while (SDL_PollEvent(&Event))
			{
				if (Event.type == SDL_QUIT)
					bQuit = true;
				else if ((Event.type == SDL_KEYDOWN) &&
					(Event.key.repeat == 0))
				{
					// Check for escape... derp
					if (Event.key.keysym.sym == SDLK_ESCAPE)
						bQuit = true;
					// Update camera movement vector
					else if (Event.key.keysym.sym == SDLK_w)
						g_SceneData.m_LocalCamera.m_fMoveZ += 8.0f;
					else if (Event.key.keysym.sym == SDLK_a)
						g_SceneData.m_LocalCamera.m_fMoveX -= 8.0f;
					else if (Event.key.keysym.sym == SDLK_s)
						g_SceneData.m_LocalCamera.m_fMoveZ -= 8.0f;
					else if (Event.key.keysym.sym == SDLK_d)
						g_SceneData.m_LocalCamera.m_fMoveX += 8.0f;
					// Update texture filtering
					else if (Event.key.keysym.sym == SDLK_1)
					{
						// No filtering
						for (int i = 0; i < 9; i++)
						{
							glBindTexture(GL_TEXTURE_2D, g_uiTextures[i]);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
						}
					}
					else if (Event.key.keysym.sym == SDLK_2)
					{
						// BiLinear
						for (int i = 0; i < 9; i++)
						{
							glBindTexture(GL_TEXTURE_2D, g_uiTextures[i]);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
						}
					}
					else if (Event.key.keysym.sym == SDLK_3)
					{
						// Mipmap
						for (int i = 0; i < 9; i++)
						{
							glBindTexture(GL_TEXTURE_2D, g_uiTextures[i]);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_NEAREST);
						}
					}
					else if (Event.key.keysym.sym == SDLK_4)
					{
						// TriLinear - Mipmap
						for (int i = 0; i < 9; i++)
						{
							glBindTexture(GL_TEXTURE_2D, g_uiTextures[i]);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
						}
					}
					else if (Event.key.keysym.sym == SDLK_5)
					{
						// TriLinear - Mipmap >> then Anisotropic
						for (int i = 0; i < 9; i++)
						{
							glBindTexture(GL_TEXTURE_2D, g_uiTextures[i]);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
							glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 2);
						}
					}
					else if (Event.key.keysym.sym == SDLK_6)
					{
						// TriLinear - Mipmap >> then Anisotropic
						for (int i = 0; i < 9; i++)
						{
							glBindTexture(GL_TEXTURE_2D, g_uiTextures[i]);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
							glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
							glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 4);
						}
					}
					else if (Event.key.keysym.sym == SDLK_SLASH)
					{
						g_bRelativeMouse = !g_bRelativeMouse;
					}
				}
				else if (Event.type == SDL_KEYUP)
				{
					// Reset camera movement vector for justice and glory
					if (Event.key.keysym.sym == SDLK_w)
						g_SceneData.m_LocalCamera.m_fMoveZ -= 8.0f;
					else if (Event.key.keysym.sym == SDLK_a)
						g_SceneData.m_LocalCamera.m_fMoveX += 8.0f;
					else if (Event.key.keysym.sym == SDLK_s)
						g_SceneData.m_LocalCamera.m_fMoveZ += 8.0f;
					else if (Event.key.keysym.sym == SDLK_d)
						g_SceneData.m_LocalCamera.m_fMoveX -= 8.0f;
				}
				else if (Event.type == SDL_MOUSEMOTION)
				{
					// Update camera view angles
					g_SceneData.m_LocalCamera.m_fAngleX += -0.1f * fElapsedTime * Event.motion.xrel;
					// Y Coordinates are in screen space so	don't get negated
					g_SceneData.m_LocalCamera.m_fAngleY += 0.1f * fElapsedTime * Event.motion.yrel;

					// Limit camera to 140 degree total rotation... but not using % because it's a float :(
					if (g_SceneData.m_LocalCamera.m_fAngleY > 1.5f)
						g_SceneData.m_LocalCamera.m_fAngleY = 1.5f;
					else if (g_SceneData.m_LocalCamera.m_fAngleY < -1.5f)
						g_SceneData.m_LocalCamera.m_fAngleY = -1.5f;
				}
				else if (Event.type == SDL_MOUSEWHEEL)
				{
					// Calculate new FOV
					float newDeg = glm::degrees(g_SceneData.m_LocalCamera.m_fFOV) + Event.wheel.y;
					// If its within bounds, apply it
					if (newDeg > 9 && newDeg < 150)
					{
						g_SceneData.m_LocalCamera.m_fFOV = glm::radians(newDeg);
					}
				}
				else if (Event.type = SDL_KEYDOWN)
				{
					if (Event.key.keysym.sym == SDLK_ESCAPE)
					{
						bQuit = true;
					}
				}
			}

			// Update the scene (avast!)
			GL_Update(fElapsedTime);

			// Render the scene
			GL_Render();

			// Swap the back-buffer and present
			SDL_GL_SwapWindow(Window);
		}

		GL_Quit(); // Delete GL Resources
	}
	return 0;
}

bool GL_Init(int iWindowWidth, int iWindowHeight)
{
	// Allow experimental / pre-release extensions
	glewExperimental = GL_TRUE;
	GLenum GlewError = glewInit();
	if (GlewError != GLEW_OK)
	{
		SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "Failed to initialize GLEW: %s\n", glewGetErrorString(GlewError));
		return false;
	}

	// Set up initial GL attributes
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_STENCIL_TEST);

	// Create vertex and fragment shader vars
	GLuint uiVertexShader;
	GLuint uiFragmentShader;
	// Only loading GGX shader (Lab 04)
	if (!GL_LoadShaderResource(uiVertexShader, 300, GL_VERTEX_SHADER))
		return false;
	if (!GL_LoadShaderResource(uiFragmentShader, 400, GL_FRAGMENT_SHADER))
		return false;
	// Create Program with current loaded shaders
	if (!GL_LoadShaders(g_uiProgram[0], uiVertexShader, uiFragmentShader))
		return false;
	// Clean up old shaders (free mem and invalidate name)
	glDeleteShader(uiVertexShader);
	glDeleteShader(uiFragmentShader);

	// Load in scene from file
	if (!GL_LoadScene("../resources/Lab05/TutorialCatherdral.fbx", g_SceneData, iWindowWidth, iWindowHeight))
	{
		glDeleteProgram(g_uiProgram[0]);
		return false;
	}

	// Get light uniform location and set (numLights for shader)
	GLuint uiUIndex = glGetUniformLocation(g_uiProgram[0], "iNumPointLights");
	glProgramUniform1i(g_uiProgram[0], uiUIndex, g_SceneData.m_uiNumLights);
	// Link the uniform buffer used for global view projection storage
	uint32_t uiBlockIndex = glGetUniformBlockIndex(g_uiProgram[0], "CameraData");
	glUniformBlockBinding(g_uiProgram[0], uiBlockIndex, 0);
	// Link the uniform buffer used for global transform storage
	uiBlockIndex = glGetUniformBlockIndex(g_uiProgram[0], "TransformData");
	glUniformBlockBinding(g_uiProgram[0], uiBlockIndex, 1);
	// Link the uniform buffer used for point light storage
	uiBlockIndex = glGetUniformBlockIndex(g_uiProgram[0], "PointLightData");
	glUniformBlockBinding(g_uiProgram[0], uiBlockIndex, 2);

	// HUH? IT MAKES NO DIFFERENCE
	// Get the uniform location and bind to texture unit
	GLuint uiTextureU = glGetUniformLocation(g_uiProgram[0], "s2DiffuseTexture");
	glUniform1i(uiTextureU, 0);
	uiTextureU = glGetUniformLocation(g_uiProgram[0], "s2SpecularTexture");
	glUniform1i(uiTextureU, 1);
	uiTextureU = glGetUniformLocation(g_uiProgram[0], "s2RoughnessTexture");
	glUniform1i(uiTextureU, 2);

	// Bind camera UBO
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, g_SceneData.m_uiCameraUBO);
	// Transform UBO bound in render
	//....
	// Bind light UBO
	glBindBufferBase(GL_UNIFORM_BUFFER, 2, g_SceneData.m_uiPointLightUBO);

	// Specify program to use
	glUseProgram(g_uiProgram[0]);
	return true;
}

bool GL_LoadShaderResource(GLuint& uiVertexShader, int resourceNumber, int shaderType)
{
	bool success = false; // return var so there's no mem leaks
	HINSTANCE hInst = GetModuleHandle(NULL);	// create bogus handle
	HRSRC hRes = FindResource(hInst, MAKEINTRESOURCE(resourceNumber), RT_RCDATA);	// find resource data from resource file
	HGLOBAL hMem = LoadResource(hInst, hRes);	// load resource in to mem and pointer
	char * eShader = (char *)LockResource(hMem);

	success = GL_LoadShader(uiVertexShader, shaderType, eShader);	// Send shader data to next function (fail if necessary)

	FreeResource(hMem);	// Free the mem
	return success; // return success var
}
bool GL_LoadShader(GLuint & uiShader, GLenum ShaderType, const GLchar * p_cShader)
{
	// Build and link the shader program
	uiShader = glCreateShader(ShaderType);
	glShaderSource(uiShader, 1, &p_cShader, NULL);
	glCompileShader(uiShader);

	// Check for errors
	GLint iTestReturn;
	glGetShaderiv(uiShader, GL_COMPILE_STATUS, &iTestReturn);
	if (iTestReturn == GL_FALSE)
	{
		GLchar p_cInfoLog[1024];
		int32_t iErrorLength;
		glGetShaderInfoLog(uiShader, 1024, &iErrorLength, p_cInfoLog);
		SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "Failed to compile shader: %s\n", p_cInfoLog);

		// Delete the failed shader
		glDeleteShader(uiShader);
		return false;
	}
	return true;
}

bool GL_LoadShaders(GLuint & uiShader, GLuint uiVertexShader, GLuint uiFragmentShader)
{
	// Link the shaders
	uiShader = glCreateProgram();
	glAttachShader(uiShader, uiVertexShader);
	glAttachShader(uiShader, uiFragmentShader);
	glLinkProgram(uiShader);

	//Check for error in link
	GLint iTestReturn;
	glGetProgramiv(uiShader, GL_LINK_STATUS, &iTestReturn);
	if (iTestReturn == GL_FALSE)
	{
		GLchar p_cInfoLog[1024];
		int32_t iErrorLength;
		glGetShaderInfoLog(uiShader, 1024, &iErrorLength, p_cInfoLog);
		SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "Failed to link shaders: %s\n", p_cInfoLog);
		glDeleteProgram(uiShader);
		return false;
	}
	return true;
}

// GL_Quit - modified for Lab02.30
void GL_Quit()
{
	GL_UnloadScene(g_SceneData);
}

void GL_Render()
{
	// Clear the render output and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Loop through each object
	for (unsigned i = 0; i < g_SceneData.m_uiNumObjects; i++)
	{
		// Bind VAO
		glBindVertexArray(g_SceneData.mp_Objects[i].m_uiVAO);
		// Bind the Transform UBO
		glBindBufferBase(GL_UNIFORM_BUFFER, 1, g_SceneData.mp_Objects[i].m_uiTransformUBO);
		// Bind the textures to texture units 
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, g_SceneData.mp_Objects[i].m_uiDiffuse);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, g_SceneData.mp_Objects[i].m_uiSpecular);
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, g_SceneData.mp_Objects[i].m_uiRough);

		// Draw the Cube
		glDrawElements(GL_TRIANGLES, g_SceneData.mp_Objects[i].m_uiNumIndices,
			GL_UNSIGNED_INT, 0);
	}

}

void GL_Update(float fElapsedTime)
{
	// Calculate new rotation value
	g_objectRotation = fElapsedTime * degreesPerSecond;

	// Update the cameras position
	g_SceneData.m_LocalCamera.m_v3Position += g_SceneData.m_LocalCamera.m_fMoveZ * fElapsedTime * g_SceneData.m_LocalCamera.m_v3Direction;
	g_SceneData.m_LocalCamera.m_v3Position += g_SceneData.m_LocalCamera.m_fMoveX * fElapsedTime * g_SceneData.m_LocalCamera.m_v3Right;

	// Determine rotation matrix for camera angles
	glm::mat4 m4Rotation = glm::rotate(glm::mat4(1.0f), g_SceneData.m_LocalCamera.m_fAngleX, glm::vec3(0.0f, 1.0f, 0.0f));
	m4Rotation = glm::rotate(m4Rotation, g_SceneData.m_LocalCamera.m_fAngleY, glm::vec3(1.0f, 0.0f, 0.0f));

	// Determine new view and right vectors
	g_SceneData.m_LocalCamera.m_v3Direction = glm::mat3(m4Rotation) * glm::vec3(0.0f, 0.0f, 1.0f);
	g_SceneData.m_LocalCamera.m_v3Right = glm::mat3(m4Rotation) * glm::vec3(-1.0f, 0.0f, 0.0f);

	// Set Mouse capture and hide cursor
	SDL_ShowCursor(g_bRelativeMouse ? 0 : 1);
	SDL_SetRelativeMouseMode(g_bRelativeMouse ? SDL_TRUE : SDL_FALSE); // FOR DEBUG set to false

	// Re-Create camera view matrix
	glm::mat4 m4View = glm::lookAt(
		g_SceneData.m_LocalCamera.m_v3Position,
		g_SceneData.m_LocalCamera.m_v3Position + g_SceneData.m_LocalCamera.m_v3Direction,
		glm::cross(g_SceneData.m_LocalCamera.m_v3Right, g_SceneData.m_LocalCamera.m_v3Direction)
		);
	// Re-Create camera projection matrix
	glm::mat4 m4Projection = glm::perspective(
		g_SceneData.m_LocalCamera.m_fFOV,
		g_SceneData.m_LocalCamera.m_fAspect,
		g_SceneData.m_LocalCamera.m_fNear, g_SceneData.m_LocalCamera.m_fFar
		);
	// Create ViewProjection matrix
	CameraData Camera = {
		m4Projection * m4View,						// View Projection
		glm::vec4(g_SceneData.m_LocalCamera.m_v3Position, 1.0)	// Position
	};
	// Update the camera buffer
	glBindBuffer(GL_UNIFORM_BUFFER, g_SceneData.m_uiCameraUBO);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(CameraData), &Camera, GL_STREAM_DRAW);
}

bool GL_LoadTextureBMP(GLuint uiTexture, const char * p_cTextureFile)
{
	// Load texture data - returns pointer with data / res / depth / ??
	SDL_Surface * p_Surface = SDL_LoadBMP(p_cTextureFile);
	if (p_Surface == NULL)
	{
		SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "Failed to open texture file : %s\n", SDL_GetError());
		return false;
	}

	// Determine image format (or error)
	GLenum Format;
	GLint iInternalFormat;
	if (p_Surface->format->BytesPerPixel == 4)
	{
		iInternalFormat = GL_RGBA;
		if (p_Surface->format->Rmask == 0x000000ff)
			Format = GL_RGBA;
		else
			Format = GL_BGRA;
	}
	else if (p_Surface->format->BytesPerPixel == 3)
	{
		iInternalFormat = GL_RGBA;
		if (p_Surface->format->Rmask == 0x000000ff)
			Format = GL_RGB;
		else
			Format = GL_BGR;
	}
	else if (p_Surface->format->BytesPerPixel == 1)
	{
		iInternalFormat = GL_RED;
		Format = GL_RED;
	}
	else
	{
		SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "Unknown texture format : %d\n", p_Surface->format->BytesPerPixel);
		return false;
	}

	// Correctly flip image data
	const int iRowSize = p_Surface->w * p_Surface->format->BytesPerPixel;
	const int iImageSize = iRowSize * p_Surface->h;
	GLubyte * p_TextureData = (GLubyte*)malloc(iImageSize);
	for (int i = 0; i < p_Surface->h * iRowSize; i += iRowSize)
	{
		memcpy(&p_TextureData[i], &((GLubyte*)p_Surface->pixels)[iImageSize - i], iRowSize);
	}

	// Bind texture
	glBindTexture(GL_TEXTURE_2D, uiTexture);

	// Copy data into texture
	glTexImage2D(GL_TEXTURE_2D, 0, iInternalFormat, p_Surface->w, p_Surface->h, 0, Format, GL_UNSIGNED_BYTE, p_TextureData);
	// Unload the SDL surface
	SDL_FreeSurface(p_Surface);
	free(p_TextureData);

	// Generate mipmaps
	glGenerateMipmap(GL_TEXTURE_2D);

	// Initialise the texture filtering values
	// No Filtering
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	// MSAA x4
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 4);

	return true;
}

bool GL_LoadTextureKTX(GLuint uiTexture, const char * p_cTextureFile)
{
	// Load texture data
	GLenum GLTarget, GLError;
	GLboolean bIsMipmapped;
	KTX_error_code ktxerror = ktxLoadTextureN(p_cTextureFile, &uiTexture, &GLTarget, NULL, &bIsMipmapped, &GLError, 0, NULL);
	if (ktxerror != KTX_SUCCESS)
	{
		SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "Failed to read texture file : %s\n", ktxErrorString(ktxerror));
		return false;
	}

	// Generate mipmaps 
	if (!bIsMipmapped)
		glGenerateMipmap(GL_TEXTURE_2D);

	// No Filtering
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	// MSAA x4
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 4);

	return true;
}

bool GL_LoadScene(const char * p_cSceneFile, SceneData & SceneInfo, int iWindowWidth, int iWindowHeight)
{
	// Using Assimp C library instead of C++
	// Load scene from file
	const aiScene * p_Scene = aiImportFile(p_cSceneFile, aiProcess_GenSmoothNormals | aiProcess_CalcTangentSpace | aiProcess_Triangulate | aiProcess_SortByPType | aiProcess_ImproveCacheLocality);
	// Check if import failed
	if (!p_Scene)
	{
		SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "Failed to open scene file: %s\n", aiGetErrorString());
		return false;
	}

	// Get import file base path
	char * p_cPath = (char *)malloc(255);
	*p_cPath = '\0';
	unsigned uiPathLength = 0;
	const char * p_cDirSlash = strrchr(p_cSceneFile, '/');
	if (p_cDirSlash != NULL)
	{
		uiPathLength = p_cDirSlash - p_cSceneFile + 1;
		strncat(p_cPath, p_cSceneFile, uiPathLength);
	}

	// Allocate buffers for each mesh
	SceneInfo.mp_Meshes = (MeshData *)realloc(SceneInfo.mp_Meshes, p_Scene->mNumMeshes * sizeof(MeshData));
	// Load in each mesh
	for (unsigned i = 0; i < p_Scene->mNumMeshes; i++)
	{
		MeshData * p_Mesh = &SceneInfo.mp_Meshes[i];
		const aiMesh * p_AIMesh = p_Scene->mMeshes[i];

		// Generate the buffers
		glGenVertexArrays(1, &p_Mesh->m_uiVAO);
		glGenBuffers(1, &p_Mesh->m_uiVBO);
		glGenBuffers(1, &p_Mesh->m_uiIBO);

		// Create the new mesh data
		p_Mesh->m_uiNumIndices = p_AIMesh->mNumFaces * 3;
		unsigned uiSizeVertices = p_AIMesh->mNumVertices * sizeof(CustomVertex);
		unsigned uiSizeIndices = p_Mesh->m_uiNumIndices * sizeof(GLuint);
		CustomVertex * p_VBuffer = (CustomVertex *)malloc(uiSizeVertices);
		GLuint * p_IBuffer = (GLuint *)malloc(uiSizeIndices);

		// Load in vertex data
		CustomVertex * p_vBuffer = p_VBuffer;
		for (unsigned j = 0; j < p_AIMesh->mNumVertices; j++)
		{
			p_vBuffer->v4Position = glm::vec4(p_AIMesh->mVertices[j].x, p_AIMesh->mVertices[j].y, p_AIMesh->mVertices[j].z, 1.0f);
			p_vBuffer->v4Normal = glm::vec4(p_AIMesh->mNormals[j].x, p_AIMesh->mNormals[j].y, p_AIMesh->mNormals[j].z, 0.0f);
			p_vBuffer->v2UV = glm::vec2(p_AIMesh->mTextureCoords[0][j].x, p_AIMesh->mTextureCoords[0][j].y);
			++p_vBuffer;
		}

		// Load in Indexes
		GLuint * p_iBuffer = p_IBuffer;
		for (unsigned j = 0; j < p_AIMesh->mNumFaces; j++)
		{
			*p_iBuffer++ = p_AIMesh->mFaces[j].mIndices[0];
			*p_iBuffer++ = p_AIMesh->mFaces[j].mIndices[1];
			*p_iBuffer++ = p_AIMesh->mFaces[j].mIndices[2];
		}

		// Bind the new Object VAO
		glBindVertexArray(p_Mesh->m_uiVAO);
		// Fill Vertex Buffer Object
		glBindBuffer(GL_ARRAY_BUFFER, p_Mesh->m_uiVBO);
		glBufferData(GL_ARRAY_BUFFER, uiSizeVertices, p_VBuffer, GL_STATIC_DRAW);
		// Fill Index Buffer Object
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, p_Mesh->m_uiIBO); // PROBLEM <-- typo here?
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, p_Mesh->m_uiNumIndices * sizeof(uiSizeIndices), p_IBuffer, GL_STATIC_DRAW);
		// Specify location of data within buffer
		// - Location of vertices
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(CustomVertex), (const GLvoid *)0);
		glEnableVertexAttribArray(0);
		// - Location of normals
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(CustomVertex), (const GLvoid *)offsetof(CustomVertex, v4Normal));
		glEnableVertexAttribArray(1);
		// - UV coordinates
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(CustomVertex), (const GLvoid *)offsetof(CustomVertex, v2UV));
		glEnableVertexAttribArray(2);

		// Cleanup allocated data
		free(p_VBuffer);
		free(p_IBuffer);

		++SceneInfo.m_uiNumMeshes;
	}

	// Allocate buffers for each material
	SceneInfo.mp_Materials = (MaterialData *)realloc(SceneInfo.mp_Materials, p_Scene->mNumMaterials * sizeof(MaterialData));
	// Load in each material
	for (unsigned i = 0; i < p_Scene->mNumMaterials; i++)
	{
		MaterialData * p_Material = &SceneInfo.mp_Materials[i];
		// Generate the buffers
		glGenTextures(3, &p_Material->m_uiDiffuse);
		// Get each texture from scene and load
		aiString sTexture;
		aiGetMaterialTexture(p_Scene->mMaterials[i], aiTextureType_DIFFUSE, 0, &sTexture);
		strcpy(&p_cPath[uiPathLength], sTexture.data); // Add scene path to filename
		GL_LoadTextureKTX(p_Material->m_uiDiffuse, p_cPath);

		aiGetMaterialTexture(p_Scene->mMaterials[i], aiTextureType_SPECULAR, 0, &sTexture);
		strcpy(&p_cPath[uiPathLength], sTexture.data); // Add scene path to filename
		GL_LoadTextureKTX(p_Material->m_uiSpecular, p_cPath);

		aiGetMaterialTexture(p_Scene->mMaterials[i], aiTextureType_SHININESS, 0, &sTexture);
		strcpy(&p_cPath[uiPathLength], sTexture.data); // Add scene path to filename
		GL_LoadTextureKTX(p_Material->m_uiRough, p_cPath);

		++SceneInfo.m_uiNumMaterials;
	}

	// Allocate buffers for each light
	SceneInfo.mp_Lights = (PointLightData *)realloc(SceneInfo.mp_Lights, p_Scene->mNumLights * sizeof(PointLightData));
	// Load in each light
	for (unsigned i = 0; i < p_Scene->mNumLights; i++)
	{
		PointLightData * p_Light = &SceneInfo.mp_Lights[i];
		const aiLight * p_AILight = p_Scene->mLights[i];
		if (p_AILight->mType == aiLightSource_POINT || p_AILight->mType == aiLightSource_SPOT)
		{
			p_Light->m_v4Position = glm::vec4(p_AILight->mPosition.x, p_AILight->mPosition.y, p_AILight->mPosition.z, 1.0f);
			p_Light->m_v4Colour = glm::vec4(p_AILight->mColorDiffuse.r, p_AILight->mColorDiffuse.g, p_AILight->mColorDiffuse.b, 1.0f);
			p_Light->m_v3Falloff = glm::vec3((p_AILight->mAttenuationConstant == 0.0f) ? 1.0f : p_AILight->mAttenuationConstant,
				p_AILight->mAttenuationLinear,
				p_AILight->mAttenuationQuadratic);
			++SceneInfo.m_uiNumLights;
		}
	}

	// Create point light UBO
	// Generate buffers if this is the first light
	if (SceneInfo.m_uiPointLightUBO == 0)
		glGenBuffers(1, &SceneInfo.m_uiPointLightUBO);

	glBindBuffer(GL_UNIFORM_BUFFER, SceneInfo.m_uiPointLightUBO);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(PointLightData) * SceneInfo.m_uiNumLights, SceneInfo.mp_Lights, GL_STREAM_DRAW); // Fixed numLights

	// Load in camera
	if (p_Scene->mNumCameras > 0)
	{
		const aiCamera * p_AICamera = p_Scene->mCameras[0];
		SceneInfo.m_LocalCamera.m_v3Position = glm::vec3(p_AICamera->mPosition.x, p_AICamera->mPosition.y, p_AICamera->mPosition.z);
		// Assimp camera data requires normalizing before use
		const glm::vec3 v3Direction = glm::normalize(glm::vec3(p_AICamera->mLookAt.x, p_AICamera->mLookAt.y, p_AICamera->mLookAt.z));
		SceneInfo.m_LocalCamera.m_v3Direction = v3Direction;
		const glm::vec3 v3Up = glm::normalize(glm::vec3(p_AICamera->mUp.x, p_AICamera->mUp.y, p_AICamera->mUp.z));
		// Assimp doesnt store a right vector so we calculate from up and direction
		SceneInfo.m_LocalCamera.m_v3Right = glm::cross(v3Direction, v3Up);
		// Use orientation vectors to calculate corresponding axis angles
		SceneInfo.m_LocalCamera.m_fAngleX = acos(glm::dot(v3Direction, glm::vec3(1.0f, 0.0f, 0.0f)));
		SceneInfo.m_LocalCamera.m_fAngleX *= (v3Direction.x > 0.0f) ? -1.0f : 1.0f;
		SceneInfo.m_LocalCamera.m_fAngleY = acos(glm::dot(v3Up, glm::vec3(0.0f, 1.0f, 0.0f)));
		SceneInfo.m_LocalCamera.m_fAngleY *= (v3Direction.y < 0.0f) ? -1.0f : 1.0f;
		SceneInfo.m_LocalCamera.m_fMoveZ = 0.0f;
		SceneInfo.m_LocalCamera.m_fMoveX = 0.0f;
		SceneInfo.m_LocalCamera.m_fFOV = p_AICamera->mHorizontalFOV;
		SceneInfo.m_LocalCamera.m_fAspect = (float)iWindowWidth / (float)iWindowHeight;
		SceneInfo.m_LocalCamera.m_fNear = p_AICamera->mClipPlaneNear;
		SceneInfo.m_LocalCamera.m_fFar = p_AICamera->mClipPlaneFar;
	}
	else if (SceneInfo.m_uiCameraUBO == 0)
	{
		// Initialise camera data
		SceneInfo.m_LocalCamera.m_fAngleX = (float)M_PI;
		SceneInfo.m_LocalCamera.m_fAngleY = 0.0f;
		SceneInfo.m_LocalCamera.m_v3Position = glm::vec3(0.0f, 0.0f, 12.0f);
		SceneInfo.m_LocalCamera.m_v3Direction = glm::vec3(0.0f, 0.0f, -1.0f);
		SceneInfo.m_LocalCamera.m_v3Right = glm::vec3(1.0f, 0.0f, 0.0f);

		// Initialise camera projection values
		SceneInfo.m_LocalCamera.m_fFOV = glm::radians(45.0f);
		SceneInfo.m_LocalCamera.m_fAspect = (float)iWindowWidth / (float)iWindowHeight;
		SceneInfo.m_LocalCamera.m_fNear = 0.1f;
		SceneInfo.m_LocalCamera.m_fFar = 100.0f;
	}

	// Create camera UBO
	glGenBuffers(1, &SceneInfo.m_uiCameraUBO);
	// Bind camera UBO
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, SceneInfo.m_uiCameraUBO);

	// Allocate buffers for each object
	unsigned uiNumBackup = SceneInfo.m_uiNumObjects;
	GL_LoadSceneNode(p_Scene->mRootNode, p_Scene, SceneInfo, glm::mat4(1.0f), false);
	SceneInfo.mp_Objects = (ObjectData *)realloc(SceneInfo.mp_Objects, SceneInfo.m_uiNumObjects * sizeof(ObjectData));
	SceneInfo.m_uiNumObjects = uiNumBackup; // Reset
	// Load in each object
	GL_LoadSceneNode(p_Scene->mRootNode, p_Scene, SceneInfo, glm::mat4(1.0f), true);

	// Destroy the scene
	aiReleaseImport(p_Scene);
	free(p_cPath);
	return true;
}

void GL_LoadSceneNode(aiNode * p_Node, const aiScene * p_Scene, SceneData & SceneInfo, const glm::mat4 & m4Transform, bool bAddObjects)
{
	// Update current transform
	glm::mat4 m4CurrentTransform = glm::transpose(*(glm::mat4 *)&p_Node->mTransformation) * m4Transform;
	// Loop over each mesh in the node
	for (unsigned i = 0; i < p_Node->mNumMeshes; i++)
	{
		// Load in each nodes mesh as an object
		if (bAddObjects)
		{
			ObjectData * p_Object = &SceneInfo.mp_Objects[SceneInfo.m_uiNumObjects];
			// Get data from corresponding mesh
			const MeshData * p_Mesh = &SceneInfo.mp_Meshes[p_Node->mMeshes[i]];
			p_Object->m_uiVAO = p_Mesh->m_uiVAO;
			p_Object->m_uiNumIndices = p_Mesh->m_uiNumIndices;

			// Get data from corresponding material
			const MaterialData * p_Material = &SceneInfo.mp_Materials[p_Scene->mMeshes[p_Node->mMeshes[i]]->mMaterialIndex];
			p_Object->m_uiDiffuse = p_Material->m_uiDiffuse;
			p_Object->m_uiSpecular = p_Material->m_uiSpecular;
			p_Object->m_uiRough = p_Material->m_uiRough;
			p_Object->m_4Transform = m4CurrentTransform;

			glGenBuffers(1, &p_Object->m_uiTransformUBO);
			glBindBuffer(GL_UNIFORM_BUFFER, p_Object->m_uiTransformUBO);
			glBufferData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), &p_Object->m_4Transform, GL_STREAM_DRAW);
		}
		++SceneInfo.m_uiNumObjects;
	}
	// Loop over each child node
	for (unsigned i = 0; i < p_Node->mNumChildren; i++)
	{
		GL_LoadSceneNode(p_Node->mChildren[i], p_Scene, SceneInfo, m4CurrentTransform, bAddObjects);
	}
}

void GL_UnloadScene(SceneData & SceneInfo)
{
	// Delete VBOs/IBOs and VAOs
	for (unsigned i = 0; i < SceneInfo.m_uiNumMeshes; i++)
	{
		glDeleteBuffers(1, &SceneInfo.mp_Meshes[i].m_uiVBO);
		glDeleteBuffers(1, &SceneInfo.mp_Meshes[i].m_uiIBO);
		glDeleteVertexArrays(1, &SceneInfo.mp_Meshes[i].m_uiVAO);
	}
	free(SceneInfo.mp_Meshes);

	// Delete materials
	for (unsigned i = 0; i < SceneInfo.m_uiNumMaterials; i++)
	{
		glDeleteTextures(3, &SceneInfo.mp_Materials[i].m_uiDiffuse);
	}
	free(SceneInfo.mp_Materials);

	// Delete objects
	for (unsigned i = 0; i < SceneInfo.m_uiNumObjects; i++)
	{
		glDeleteBuffers(1, &SceneInfo.mp_Objects[i].m_uiTransformUBO);
	}
	free(SceneInfo.mp_Objects);

	// Delete light UBO
	glDeleteBuffers(1, &SceneInfo.m_uiPointLightUBO);
	free(SceneInfo.mp_Lights);

	// Delete camera UBO
	glDeleteBuffers(1, &SceneInfo.m_uiCameraUBO);
}