#version 440
// Defines //
/////////////
#define M_RCPPI 0.31830988618379067153776752674503
#define M_PI 3.1415926535897932384626433832795

// Data Structures //
/////////////////////
struct PointLight { 
	vec3 v3LightPosition; 
	vec4 v4LightIntensity; 
	vec3 v3Falloff; 
};

// Inputs
layout( location = 0 ) in vec4 v4Position;
layout( location = 1 ) in vec4 v4Normal;
layout( location = 2 ) in vec2 v2UV;
// Outputs
out vec4 v4ColourOut;

// Allow up to MAX_LIGHTS light sources
#define MAX_LIGHTS 8
uniform PointLightData {
	PointLight PointLights[MAX_LIGHTS];
};
uniform int iNumPointLights;

// Add in uniforms for textures (instead of materials)
uniform sampler2D s2DiffuseTexture; 
uniform sampler2D s2SpecularTexture; 
uniform sampler2D s2RoughnessTexture;
layout( binding = 3 ) uniform samplerCube scRefractMapTexture; // Refraction / Transparency cubemap

uniform CameraData {
	mat4 m4ViewProjection;
	vec3 v3CameraPosition;
};

// Function Prototypes //
/////////////////////////
vec3 lightFalloff( in vec3 v3LightIntensity, in vec3 v3Falloff, in vec3 v3LightPosition, in vec3 v3Position ); // Both
vec3 schlickFresnel( in vec3 v3LightDirection, in vec3 v3HalfVector, in vec4 v4SpecularColour );
float TRDistribution( in vec3 v3Normal, in vec3 v3HalfVector, in float fRoughness );			// for GGX
float GGXVisibility( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in float fRoughness );	// for GGX
vec3 GGX( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in vec3 v3LightIrradiance, in vec4 v4DiffuseColour, in vec4 v4SpecularColour, in float fRoughness ); // for GGX
vec3 SpecularTransmit( in vec3 v3Normal, in vec3 v3ViewDirection, in vec4 v4DiffuseColour, in vec4 v4SpecularColour );
vec3 GGXReflect( in vec3 v3Normal, in vec3 v3ReflectDirection, in vec3 v3ViewDirection, in vec3 v3ReflectRadiance, in vec4 v4SpecularColour, in float fRoughness );

// Add uniform for emissive objects power
layout( location = 1 ) uniform float fEmissivePower;
// Set up subroutine for emissive objects
subroutine vec3 Emissive( vec3, vec4 );
// Hardcode subroutine //
layout( index = 0 ) subroutine(Emissive) vec3 noEmissive( vec3 v3ColourOut, vec4 v4DiffuseColour )
{
	return v3ColourOut; // Return colour unmodified
}
layout( index = 1 ) subroutine(Emissive) vec3 textureEmissive( vec3 v3ColourOut, vec4 v4DiffuseColour )
{
	return v3ColourOut + ( fEmissivePower * v4DiffuseColour.xyz ); // Add in emissive contribution
}
layout( location = 0 ) subroutine uniform Emissive EmissiveUniform;


// Set up subroutine for transparent materials
subroutine vec3 RefractMap( vec3, vec3, vec3, vec4, vec4 );
layout( index = 2 ) subroutine(RefractMap) vec3 noRefractMap( vec3 v3ColourOut, vec3 v3Normal, vec3 v3ViewDirection, vec4 v4DiffuseColour, vec4 v4SpecularColour )
{
	// Return colour unmodified
	return v3ColourOut;
}
layout( index = 3 ) subroutine(RefractMap) vec3 textureRefractMap( vec3 v3ColourOut, vec3 v3Normal, vec3 v3ViewDirection, vec4 v4DiffuseColour, vec4 v4SpecularColour )
{
	// Get specular transmittance term
	vec3 v3Transmit = SpecularTransmit( v3Normal, v3ViewDirection,v4DiffuseColour, v4SpecularColour );
	// Add in transparent contribution and blend with existing
	return mix( v3Transmit, v3ColourOut, v4DiffuseColour.w );
}
layout( location = 1 ) subroutine uniform RefractMap RefractMapUniform;

// Create additional texture input for object's reflection texture
layout( binding = 4 ) uniform sampler2D s2ReflectTexture;
// UBO for data required to map the texture to the planes surface
layout( binding = 3 ) uniform ReflectPlaneData
{
	mat4 m4ReflectVP;
};

// Set up subroutine for reflective materials
subroutine vec3 ReflectMap( vec3, vec3, vec3, vec4, float );

layout( index = 4 ) subroutine(ReflectMap) vec3 noReflectMap( vec3 v3ColourOut, vec3 v3Normal, vec3 v3ViewDirection, vec4 v4SpecularColour, float fRoughness )
{
	return v3ColourOut;
}

// Linear (plane) reflection
layout( index = 5 ) subroutine(ReflectMap) vec3 textureReflectPlane( vec3 v3ColourOut, vec3 v3Normal, vec3 v3ViewDirection, vec4 v4SpecularColour, float fRoughness )
{
	// Get position in reflection texture
	vec4 v4RVPPosition = m4ReflectVP * v4Position;
	vec2 v2ReflectUV = v4RVPPosition.xy / v4RVPPosition.w;
	v2ReflectUV = ( v2ReflectUV + 1.0 ) * 0.5;
	
	// Calculate LOD offset
	// - Change LOD based on roughness, gives a blurrier reflection
	float fLOD = textureQueryLod( s2ReflectTexture, v2ReflectUV ).y;
	float fGloss = 1.0 - fRoughness;
	fLOD += ( ( 2.0 / ( fGloss * fGloss ) ) - 1.0 );
	
	// Get reflect texture data
	vec3 v3ReflectRadiance= textureLod( s2ReflectTexture, v2ReflectUV, fLOD ).xyz;

	// Get reflect direction
	vec3 v3ReflectDirection = normalize( reflect( -v3ViewDirection, v3Normal ) );
	
	// Perform Shading
	vec3 v3RetColour = GGXReflect( v3Normal, v3ReflectDirection, v3ViewDirection, v3ReflectRadiance, v4SpecularColour, fRoughness );

	return v3ColourOut + v3RetColour;
}

// Create additional texture input for object's cubemap reflection texture
layout( binding = 5 ) uniform samplerCube scReflectMapTexture;
layout( index = 6 ) subroutine(ReflectMap) vec3 textureReflectCube( vec3 v3ColourOut, vec3 v3Normal, vec3 v3ViewDirection, vec4 v4SpecularColour, float fRoughness )
{
	// Get reflect direction
	vec3 v3ReflectDirection = normalize( reflect( -v3ViewDirection, v3Normal ) );

	// Calculate LOD offset
	float fLOD = textureQueryLod( scReflectMapTexture, v3ReflectDirection ).y;
	float fGloss = 1.0 - fRoughness;
	fLOD += ( ( 2.0 / ( fGloss * fGloss ) ) - 1.0 );

	// Get reflect texture data
	vec3 v3ReflectRadiance = textureLod( scReflectMapTexture, v3ReflectDirection, fLOD ).xyz;

	// Perform shading
	vec3 v3RetColour = GGXReflect( v3Normal, v3ReflectDirection, v3ViewDirection, v3ReflectRadiance, v4SpecularColour, fRoughness );

	return v3ColourOut + v3RetColour;
	//return v3ColourOut;
}
layout( location = 2 ) subroutine uniform ReflectMap ReflectMapUniform;

// Main //
//////////
void main() {
	// Normalize the inputs
	vec3 v3TNormal = normalize( v4Normal.xyz );
	vec3 v3TViewDir = normalize( v3CameraPosition - v4Position.xyz );

	// Get texture data
	vec4 v4DiffuseColour = texture( s2DiffuseTexture, v2UV );
	vec4 v4SpecularColour = texture( s2SpecularTexture, v2UV );
	float fRoughness = texture( s2RoughnessTexture, v2UV ).r;

	// Create / Initialise vars for lighting
	vec3 v3ColourOut = vec3(0.0, 0.0, 0.0);

	for(int i = 0; i < iNumPointLights; ++i)
	{
		// Normalize light direction
		vec3 v3TLightDir = normalize( PointLights[i].v3LightPosition - v4Position.xyz );
		// Calculate light falloff
		vec3 v3LightIrradiance = lightFalloff( PointLights[i].v4LightIntensity.rgb, PointLights[i].v3Falloff, PointLights[i].v3LightPosition, v4Position.xyz );
		// Perform shading and ADD to current output
		v3ColourOut += GGX( v3TNormal, v3TLightDir, v3TViewDir, v3LightIrradiance, v4DiffuseColour, v4SpecularColour, fRoughness );
	}
	// Add Diffuse Lighting
	v3ColourOut += v4DiffuseColour.xyz * vec3( 0.14, 0.14, 0.14 );
	// Add in any emissive contribution
	v3ColourOut = EmissiveUniform( v3ColourOut, v4DiffuseColour );
	// Add in any refraction contribution
	v3ColourOut = RefractMapUniform( v3ColourOut, v3TNormal, v3TViewDir, v4DiffuseColour, v4SpecularColour );
	// Add in any reflection contribution
	v3ColourOut = ReflectMapUniform( v3ColourOut, v3TNormal, v3TViewDir, v4SpecularColour, fRoughness );
	// Output colour information
	v4ColourOut = vec4( v3ColourOut, 1.0 );
}

// Functions //
///////////////
vec3 lightFalloff( in vec3 v3LightIntensity, in vec3 v3Falloff, in vec3 v3LightPosition, in vec3 v3Position )
{
	// Calculate distance from light
	float fDist = distance( v3LightPosition, v3Position );
	// Return fallof
	//return v3LightIntensity / ( fFalloff * fDist * fDist );
	return v3LightIntensity / (( v3Falloff[0] ) + ( v3Falloff[1] * fDist ) + ( v3Falloff[2] * fDist * fDist ));
}

vec3 schlickFresnel( in vec3 v3LightDirection, in vec3 v3HalfVector, in vec4 v4SpecularColour )
{
	// Schlick Fresnel approximation
	float fLH = dot( v3LightDirection, v3HalfVector );
	return v4SpecularColour.rgb + ( 1.0 - v4SpecularColour.rgb ) * pow( 1.0 - fLH, 5 );
}

float TRDistribution( in vec3 v3Normal, in vec3 v3HalfVector, in float fRoughness )
{
	// Trowbridge-Reitz Distribution function
	float fNSq = fRoughness * fRoughness;
	float fNH = max( dot( v3Normal, v3HalfVector ), 0.0 );
	float fDenom = fNH * fNH * ( fNSq - 1 ) + 1;
	return fNSq / ( M_PI * fDenom * fDenom );
}

float GGXVisibility( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in float fRoughness )
{
	// GGX Visibility function
	float fNL = max( dot( v3Normal, v3LightDirection ), 0.0 );
	float fNV = max( dot( v3Normal, v3ViewDirection ), 0.0 );
	float fRSq = fRoughness * fRoughness;
	float fRMod = 1.0 - fRSq;
	float fRecipG1 = fNL + sqrt( fRSq + ( fRMod * fNL * fNL ) );
	float fRecipG2 = fNV + sqrt( fRSq + ( fRMod * fNV * fNV ) );
	return 1.0 / ( fRecipG1 * fRecipG2 );
}

vec3 GGX( in vec3 v3Normal, in vec3 v3LightDirection, in vec3 v3ViewDirection, in vec3 v3LightIrradiance, in vec4 v4DiffuseColour, in vec4 v4SpecularColour, in float fRoughness )
{
	// Calculate half vector
	vec3 v3HalfVector = normalize( v3ViewDirection + v3LightDirection );
	
	// Calculate diffuse component
	vec3 v3Diffuse = v4DiffuseColour.rgb * M_RCPPI;
	// scale diffuse term
	float Fln = max(dot( v3LightDirection, v3HalfVector), 0.0); // using half vector instead of normal, and max for correction
	v3Diffuse *= 1 - (v4DiffuseColour.xyz * Fln);

	// Calculate Toorance-Sparrow components
	vec3 v3F = schlickFresnel( v3LightDirection, v3HalfVector, v4SpecularColour );
	float fD = TRDistribution( v3Normal, v3HalfVector, fRoughness );
	float fV = GGXVisibility( v3Normal, v3LightDirection, v3ViewDirection, fRoughness );

	//Combine diffuse and specular
	vec3 v3RetColour = v3Diffuse + v3F * fD * fV;

	// Multiply by view angle
	v3RetColour *= max( dot( v3Normal, v3LightDirection ), 0.0 );
	// Combine with incoming light value
	v3RetColour *= v3LightIrradiance;

	return v3RetColour;
}

vec3 SpecularTransmit( in vec3 v3Normal, in vec3 v3ViewDirection, in vec4 v4DiffuseColour, in vec4 v4SpecularColour )
{
	// Calculate index of refraction from Fresnel term
	float fRootF0 = sqrt( v4SpecularColour.x );
	float fIOR = ( 1 - fRootF0 ) / ( 1 + fRootF0 );
	// Get refraction direction
	vec3 v3Refract = refract( -v3ViewDirection, v3Normal, fIOR );
	// Get refraction map data
	vec3 v3RefractColour = texture( scRefractMapTexture, v3Refract ).xyz;
	// Evaluate specular transmittance
	vec3 v3RetColour = fIOR * ( 1.0 - schlickFresnel( v3Refract, -v3Normal, v4SpecularColour ) );
	v3RetColour *= v4DiffuseColour.xyz;
	// Combine with incoming light value
	v3RetColour *= v3RefractColour;
	return v3RetColour;
}

vec3 GGXReflect( in vec3 v3Normal, in vec3 v3ReflectDirection, in vec3 v3ViewDirection, in vec3 v3ReflectRadiance, in vec4 v4SpecularColour, in float fRoughness )
{
	// Calculate Toorance-Sparrow components
	vec3 v3F = schlickFresnel(v3ReflectDirection, v3Normal, v4SpecularColour);
	float fV = GGXVisibility( v3Normal, v3ReflectDirection, v3ViewDirection, fRoughness );

	// Combine specular
	vec3 v3RetColour = v3F * fV;

	// Modify by pdf
	v3RetColour *= ( 4 * dot( v3ViewDirection, v3Normal ) );

	// Multiply by view angle
	v3RetColour *= max( dot( v3Normal, v3ReflectDirection ), 0.0 );

	// Combine with incoming light value
	v3RetColour *= v3ReflectRadiance;

	return v3RetColour;
}