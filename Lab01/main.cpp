// Using SDL, SDL OpenGL, GLEW
#define GLEW_STATIC
#include <GL/glew.h>
#include <SDL.h>
#include <SDL_opengl.h>

GLuint g_uiVAO;
GLuint g_uiVBO;
GLuint g_uiProgram;

bool GL_Init( int iWindowWidth, int iWindowHeight);
bool GL_LoadShader( GLuint & uiShader, GLenum ShaderType, const GLchar * p_cShader );
bool GL_LoadShaders( GLuint & uiShader, GLuint uiVertexShader, GLuint uiFragmentShader );
void GL_Quit( );
void GL_Render( );

#ifdef _WIN32
int WINAPI WinMain( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, 
				    _In_ LPSTR lpCmdLine, _In_ int )
#else
int main( int argc, char **argv )
#endif
{
	if( SDL_Init( SDL_INIT_VIDEO ) != 0 )
	{
		SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION,
		"Failed to initialize SDL: %s\n", SDL_GetError() );
		return 1;
	}

	// Use OpenGL 4.4 core profile
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 4 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 4 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK,
	SDL_GL_CONTEXT_PROFILE_CORE );
	// Turn on double buffering 24bit Z buffer
	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
	SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 24 );

	// Create a SDL window
	const int iWindowWidth = 1280;
	const int iWindowHeight = 1024;
	const bool bWindowFullscreen = false;

	// Get Desktop Resolution
	SDL_DisplayMode CurrentDisplay;
	SDL_GetCurrentDisplayMode(0, &CurrentDisplay);

	SDL_Window * Window = SDL_CreateWindow( "AGT Tutorial",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, bWindowFullscreen ? CurrentDisplay.w : iWindowWidth, bWindowFullscreen ? CurrentDisplay.h : iWindowHeight,
		SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | (bWindowFullscreen * SDL_WINDOW_FULLSCREEN) );
	if( Window == NULL )
	{
		SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION, "Failed to create OpenGL window: %s\n", SDL_GetError() );
		SDL_Quit();
		return 1;
	}

	// Create OpenGL Context
	SDL_GLContext Context = SDL_GL_CreateContext( Window );
	if( Context == NULL )
	{
		SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION, "Failed to create OpenGL context: %s\n", SDL_GetError() );
		SDL_DestroyWindow(Window);
		SDL_Quit();
		return 1;
	}

	// VSync (-1 = enable late swaps)
	SDL_GL_SetSwapInterval( -1 );

	// Initialize OpenGL
	if( GL_Init( iWindowWidth, iWindowHeight) ) // User made functions
	{
		SDL_Event Event;
		bool bQuit = false;
		while( !bQuit )
		{
			// Poll SDL for buffered events
			while( SDL_PollEvent( &Event ) )
			{
				if( Event.type == SDL_QUIT )
					bQuit = true;
				else if( Event.type = SDL_KEYDOWN )
				{
					if( Event.key.keysym.sym == SDLK_ESCAPE )
					{
						bQuit = true;
					}
				}
			}
		
			// Render the scene
			GL_Render( );

			// Swap the back-buffer and present
			SDL_GL_SwapWindow( Window );
		}

		GL_Quit(); // Delete GL Resources
	}
	return 0;
}

bool GL_Init( int iWindowWidth, int iWindowHeight)
{
	// Allow experimental / pre-release extensions
	glewExperimental = GL_TRUE;
	GLenum GlewError = glewInit( );
	if( GlewError != GLEW_OK )
	{
		SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION, "Failed to initialize GLEW: %s\n", glewGetErrorString( GlewError) );
		return false;
	}
	// Set up initial GL attributes

	glClearColor( 0.0f, 0.0f, 1.0f, 1.0f);
	glCullFace( GL_BACK );
	glEnable( GL_CULL_FACE );
	glEnable( GL_DEPTH_TEST );
	glDisable( GL_STENCIL_TEST );

	// Get vertex shader from file
	//* This is clearly the broken bit *//
	HINSTANCE hInst = GetModuleHandle(NULL); // create bogus handle
	HRSRC hRes = FindResource(hInst, MAKEINTRESOURCE(100), RT_RCDATA); // find resource data from resource file
	HGLOBAL hMem = LoadResource(hInst, hRes); // load resource in to mem and pointer
	char * eShader = (char *)LockResource(hMem);

	// Create a vertex Shader
	GLuint uiVertexShader;
	if( !GL_LoadShader( uiVertexShader, GL_VERTEX_SHADER, eShader) )
		return false;

	// Free the mem
	FreeResource(hMem);

	hInst = GetModuleHandle(NULL); // create bogus handle
	hRes = FindResource(hInst, MAKEINTRESOURCE(200), RT_RCDATA); // find resource data from resource file
	hMem = LoadResource(hInst, hRes); // load resource in to mem and pointer
	eShader = (char *)LockResource(hMem);

	// Create fragment shader source
	/*
	const GLchar p_cFragmentShaderSource[] =
	{
		"#version 440\n \
		out vec4 v4ColourOut;\n \
		void main() {\n \
			v4ColourOut = vec4(1.0,1.0,0.0,1.0);\n \
		}"
	};
	*/

	// Create fragment shader
	GLuint uiFragmentShader;
	if( !GL_LoadShader( uiFragmentShader, GL_FRAGMENT_SHADER, eShader) )
		return false;

	// Free the mem
	FreeResource(hMem);

	// Create Program
	if( !GL_LoadShaders( g_uiProgram, uiVertexShader, uiFragmentShader ) )
		return false;

	// Clean up unneeded shaders
	glDeleteShader( uiVertexShader );
	glDeleteShader( uiFragmentShader );

	// Create a Vertex Array Object
	glGenVertexArrays( 1, &g_uiVAO );
	glBindVertexArray( g_uiVAO );

	// Create VBO data
	GLfloat fVertexData[] =
	{
		0.0f, -0.5f,
		0.5f, 0.5f,
		-0.5f, 0.5f
	};

	// Create Vertex Buffer Object
	glGenBuffers( 1, &g_uiVBO );
	glBindBuffer( GL_ARRAY_BUFFER,	g_uiVBO );
	glBufferData( GL_ARRAY_BUFFER, sizeof( fVertexData ), fVertexData, GL_STATIC_DRAW );

	// Specify location of data within buffer
	glVertexAttribPointer( 0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof( GLfloat ), (const GLvoid *) 0 );
	glEnableVertexAttribArray( 0 );

	// Specify program to use
	glUseProgram( g_uiProgram );
	return true;
}

bool GL_LoadShader( GLuint & uiShader, GLenum ShaderType, const GLchar * p_cShader )
{
	// Build and link the shader program
	uiShader = glCreateShader( ShaderType );
	glShaderSource( uiShader, 1, &p_cShader, NULL );
	glCompileShader( uiShader );

	// Check for errors
	GLint iTestReturn;
	glGetShaderiv( uiShader, GL_COMPILE_STATUS, &iTestReturn );
	if( iTestReturn == GL_FALSE )
	{
		GLchar p_cInfoLog[1024];
		int32_t iErrorLength;
		glGetShaderInfoLog( uiShader, 1024, &iErrorLength, p_cInfoLog );
		SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION, "Failed to compile shader: %s\n", p_cInfoLog );

		// Delete the failed shader
		glDeleteShader( uiShader );
		return false;
	}
	return true;
}

bool GL_LoadShaders( GLuint & uiShader, GLuint uiVertexShader, GLuint uiFragmentShader )
{
	// Link the shaders
	uiShader = glCreateProgram( );
	glAttachShader( uiShader, uiVertexShader );
	glAttachShader( uiShader, uiFragmentShader );
	glLinkProgram( uiShader );

	//Check for error in link
	 GLint iTestReturn;
	 glGetProgramiv( uiShader, GL_LINK_STATUS, &iTestReturn );
	 if( iTestReturn == GL_FALSE )
	 {
		 GLchar p_cInfoLog[1024];
		 int32_t iErrorLength;
		 glGetShaderInfoLog( uiShader, 1024, &iErrorLength, p_cInfoLog );
		 SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION, "Failed to link shaders: %s\n", p_cInfoLog );
		 glDeleteProgram( uiShader );
		 return false;
	 }
	 return true;
}

void GL_Quit( )
{
	// Release the shader program
	glDeleteProgram( g_uiProgram );

	// Delete VBO and VAO
	glDeleteBuffers( 1, &g_uiVBO );
	glDeleteVertexArrays( 1, &g_uiVAO );
}

void GL_Render( )
{
	// Clear the render output and depth buffer
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	// Specify VAO to use
	glBindVertexArray( g_uiVAO );

	// Draw the triangle
	glDrawArrays( GL_TRIANGLES, 0, 3 );
}