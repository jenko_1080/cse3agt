#version 440
layout( location = 0 ) in vec2 v2VertexPos2D;
void main()
{
	gl_Position = vec4( v2VertexPos2D.x, v2VertexPos2D.y, 0.0f, 1.0f );
}