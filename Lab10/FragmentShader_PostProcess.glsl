#version 440
layout( binding = 7 ) uniform InvResolution
{ 
	vec2 v2InvResolution; 
}; 
layout( binding = 0 ) uniform sampler2D s2AccumulationTexture; 
layout( binding = 1 ) uniform sampler2D s2LuminanceKeyTexture;
layout( binding = 2 ) uniform sampler2D s2BloomTexture;

out vec3 v3ColourOut; 

float log10( in float fVal );

void main( )
{
	// Get UV coordinates
	vec2 v2UV = gl_FragCoord.xy * v2InvResolution;

	// Get colour data
	vec3 v3RetColour = texture( s2AccumulationTexture, v2UV ).rgb;
	// Get key luminance value
	float fYa = texture( s2LuminanceKeyTexture, v2UV, 1024 ).r;

	// Perform range mapping (optional: only needed with integer textures) 
	const float fEpsilon = 0.00000001; 
	const float fYwhite = 0.22;
	fYa = ( fYa * ( log( fYwhite ) - log( fEpsilon ) ) ) + log( fEpsilon ); 
	// Get exponent of log values 
	fYa = exp( fYa ); 

	// Calculate middle-grey 
	float fDg = 1.03 - ( 2 / ( 2 + log10( fYa + 1 ) ) ); 

	// Calculate current luminance 
	const vec3 v3LuminanceConvert = vec3( 0.2126, 0.7152, 0.0722 );
	float fY = dot( v3RetColour, v3LuminanceConvert );

	// Calculate relative luminance 
	float fYr = ( fDg / fYa ) * fY;

	// Calculate new luminance 
	float fYNew = ( fYr * ( 1 + ( fYr / ( fYwhite * fYwhite ) ) ) ) / ( 1 + fYr ); 

	// Perform bloom addition
	vec3 v3Bloom = texture( s2BloomTexture, v2UV, 1024 ).rgb;
	v3RetColour += ( v3Bloom * 0.98 );

	// Perform tone mapping 
	v3RetColour *= ( fYNew / fY );
	v3ColourOut = v3RetColour;
}

float log10( in float fVal ) 
{ 
	// Log10(x) = log2(x) / log2(10) 
	return log2( fVal ) * 0.30102999566374217366165225171822; 
}