#version 440
layout( binding = 7 ) uniform InvResolution
{ 
	vec2 v2InvResolution; 
}; 
layout( binding = 15 ) uniform sampler2D s2AccumulationTexture; 

out vec3 v3ColourOut; 

void main( )
{
	// Get UV coordinates
	vec2 v2UV = gl_FragCoord.xy * v2InvResolution;

	// Pass through colour data
	v3ColourOut = texture( s2AccumulationTexture, v2UV ).rgb;
}