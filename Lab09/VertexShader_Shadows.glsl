#version 440
layout( binding = 1 ) uniform TransformData {
	mat4 m4Transform;
};

layout( location = 0 ) in vec4 v4VertexPos;
layout( location = 0 ) smooth out vec4 v4Position;

void main() {
	// Transform vertex
	v4Position = m4Transform * v4VertexPos;
}
