#version 440
layout( vertices = 3 ) out;

// Inputs from vertex shader
layout( location = 0 ) in vec4 v4VertexPos[];
layout( location = 1 ) in vec4 v4VertexNormal[]; 
layout( location = 2 ) in vec2 v2VertexUV[];
layout( location = 3 ) in vec4 v4VertexTangent[];
// Passed through outputs
layout( location = 0 ) out vec4 v4Position[3]; //030, 003, 300
layout( location = 3 ) out vec4 v4Normal[3]; 
layout( location = 6 ) out vec2 v2UV[3]; 
layout( location = 9 ) out vec4 v4Tangent[3];
// PN Triangle additional data
layout( location = 12 ) out vec3 v3PatchE1[3]; //021, 102, 210
layout( location = 15 ) out vec3 v3PatchE2[3]; //012, 201, 120
layout( location = 18 ) out patch vec3 v3P111;
// Uniforms
layout( location = 3 ) uniform vec2 v2Resolution;

// Function Prototypes //
/////////////////////////
vec3 projectToPlane( in vec3 v3Point, in vec3 v3PlanePoint, in vec3 v3PlaneNormal );
float tessLevel( in vec3 v3Point1, in vec3 v3Point2 );

// Main //
//////////
void main()
{
	// Pass through the control points of the patch 
	v4Position[gl_InvocationID] = v4VertexPos[gl_InvocationID]; 
	v4Normal[gl_InvocationID] = v4VertexNormal[gl_InvocationID]; 
	v2UV[gl_InvocationID] = v2VertexUV[gl_InvocationID];
	v4Tangent[gl_InvocationID] = v4VertexTangent[gl_InvocationID];

	// Calculate Bezier patch control points
	const int iNextInvocID = gl_InvocationID < 2 ? gl_InvocationID + 1 : 0;
	vec3 v3CurrPos = v4VertexPos[gl_InvocationID].xyz;
	vec3 v3NextPos = v4VertexPos[iNextInvocID].xyz;
	vec3 v3CurrNormal = normalize( v4VertexNormal[gl_InvocationID].xyz );
	vec3 v3NextNormal = normalize( v4VertexNormal[iNextInvocID].xyz );

	// Project onto vertex normal plane
	vec3 v3ProjPoint1 = projectToPlane( v3NextPos, v3CurrPos, v3CurrNormal );
	vec3 v3ProjPoint2 = projectToPlane( v3CurrPos, v3NextPos, v3NextNormal );

	// Calculate Bezier CP at 1/3 length
	v3PatchE1[gl_InvocationID] = ( ( 2.0 * v3CurrPos ) + v3ProjPoint1 ) / 3.0;
	v3PatchE2[gl_InvocationID] = ( ( 2.0 * v3NextPos ) + v3ProjPoint2 ) / 3.0;

	barrier();
	if( gl_InvocationID == 0 )
	{
		vec4 v4PositionVPTemp[3];
		int iOutOfBound[6] = int[6]( 0, 0, 0, 0, 0, 0 );

		// Loop over each vertex and get clip space position 
		for( int i = 0; i < 3; ++i )
		{
			// Check if any value is outside clip planes
			if( gl_in[i].gl_Position.x > gl_in[i].gl_Position.w ) ++iOutOfBound[0];
			if( gl_in[i].gl_Position.x < -gl_in[i].gl_Position.w ) ++iOutOfBound[1];
			if( gl_in[i].gl_Position.y > gl_in[i].gl_Position.w ) ++iOutOfBound[2];
			if( gl_in[i].gl_Position.y < -gl_in[i].gl_Position.w ) ++iOutOfBound[3];
			if( gl_in[i].gl_Position.z > gl_in[i].gl_Position.w ) ++iOutOfBound[4];
			if( gl_in[i].gl_Position.z < -gl_in[i].gl_Position.w ) ++iOutOfBound[5];
		}
		
		// Loop over each clip face and check if triangle is completely outside
		bool bInFrustum = true;
		for( int i = 0; i < 6; ++i )
			if( iOutOfBound[i] == 3 ) bInFrustum = false;

		// If visible output triangle data
		if( bInFrustum )
		{
			// Get Bezier patch values
			vec3 v3P030 = v4VertexPos[0].xyz;
			vec3 v3P021 = v3PatchE1[0].xyz; vec3 v3P012 = v3PatchE2[0].xyz; vec3 v3P003 = v4VertexPos[1].xyz; 
			vec3 v3P102 = v3PatchE1[1].xyz; vec3 v3P201 = v3PatchE2[1].xyz; vec3 v3P300 = v4VertexPos[2].xyz; 
			vec3 v3P210 = v3PatchE1[2].xyz; vec3 v3P120 = v3PatchE2[2].xyz;

			// Calculate centre point
			vec3 v3E = ( v3P021 + v3P012 + v3P102 + v3P201 + v3P210 + v3P120 ) / 6.0f;
			vec3 v3V = ( v3P300 + v3P003 + v3P030 ) / 3.0f;
			v3P111 = v3E + ( ( v3E - v3V ) / 2.0f );

			// Check if the plane is flat
			float tolerance = 0.9;
			bool isFlat = dot( v3CurrNormal, v3NextNormal ) > ( tolerance ) && dot( v3CurrNormal,  normalize(v4VertexNormal[2].xyz) ) > ( tolerance );
			if(isFlat)
			{
				// Set tesselation levels to 1 ('no tesselation')
				gl_TessLevelOuter[0] = 1;
				gl_TessLevelOuter[1] = 1;
				gl_TessLevelOuter[2] = 1;
			}
			else
			{
				// Calculate the tessellation levels
				gl_TessLevelOuter[0] = 1;
				gl_TessLevelOuter[1] = 1;
				gl_TessLevelOuter[2] = 1;
				//gl_TessLevelOuter[0]= tessLevel( gl_in[1].gl_Position.xyz, gl_in[2].gl_Position.xyz );
				//gl_TessLevelOuter[1]= tessLevel( gl_in[2].gl_Position.xyz, gl_in[0].gl_Position.xyz );
				//gl_TessLevelOuter[2]= tessLevel( gl_in[0].gl_Position.xyz, gl_in[1].gl_Position.xyz );
			}
			// Leave this one out here to have a shorter branch for parallelismness
			gl_TessLevelInner[0] = max( gl_TessLevelOuter[0], max( gl_TessLevelOuter[1], gl_TessLevelOuter[2] ) );
		}
		else
		{
			// Discard patches by setting tessellation levels to zero
			gl_TessLevelOuter = float[]( 0, 0, 0, 0 );
			gl_TessLevelInner = float[]( 0, 0 );
		}
	}

}

vec3 projectToPlane( in vec3 v3Point, in vec3 v3PlanePoint, in vec3 v3PlaneNormal )
{
	// Project point to plane
	float fD = dot( v3Point - v3PlanePoint, v3PlaneNormal );
	vec3 v3D = fD * v3PlaneNormal;
	return v3Point - v3D;
}

float tessLevel( in vec3 v3Point1, in vec3 v3Point2 )
{
	const float fPixelsPerEdge = 4;
	vec2 v2P1 = ( v3Point1.xy + v3Point2.xy ) * 0.5;
	vec2 v2P2 = v2P1;
	v2P2.y += distance( v3Point1, v3Point2 );
	float fLength = length( ( v2P1 - v2P2 ) * v2Resolution * 0.5 );
	return clamp( fLength / fPixelsPerEdge, 1.0, 32.0 );
}
